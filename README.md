
# Advent Of Code 2020

My Advent of Code 2020 solutions (in C++).

## Dependencies

* C++ compiler.
* CMake (for easier building).

Each C++ source file is independent and thus sources
can be compiled without CMake, just check that the
puzzle input path matches that of the **INPUT_PATH**
variable on the source file.
