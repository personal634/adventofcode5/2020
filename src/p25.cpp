//
// Created by imanol on 30/04/21.
//

#include <iostream>

using namespace std;

const char* INPUT_PATH = "../input/input25";

void get_input(const char* filename, unsigned long &pkc, unsigned long &pkd) {
    // File.
    FILE* f = fopen(filename, "r");

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    char line[100];
    string s;

    fgets(line, 100, f);
    s = string(line);
    pkc = stoi(s);

    fgets(line, 100, f);
    s = string(line);
    pkd = stoi(s);

    fclose(f);
}

unsigned long transform(unsigned long sn, unsigned long cv) {
    return (sn * cv) % 20201227;
}

unsigned long solve_1(const unsigned long &pkc, const unsigned long &pkd) {
    unsigned long lsc = 0, lsd = 0, cn = 1, dn = 1;

    while (cn != pkc) {
        cn = transform(7, cn);
        ++lsc;
    }

    while (dn != pkd) {
        dn = transform(7, dn);
        ++lsd;
    }

    dn = 1;
    for (unsigned long i = 0; i < lsc; ++i) {
        dn = transform(pkd, dn);
    }

    return dn;
}

unsigned long solve_2() {
    return 0;
}

int main() {
    unsigned long n, pkc, pkd;

    // Part one.
    get_input(INPUT_PATH, pkc, pkd);
    n = solve_1(pkc, pkd);
    printf("Part 1: %lu\n", n);
    return 0;
}
