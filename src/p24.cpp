//
// Created by imanol on 30/04/21.
//

#include <iostream>
#include <set>
#include <map>

using namespace std;

const char* INPUT_PATH = "../input/input24";

unsigned long solve_1(const char* filename, set<pair<signed long, signed long>> &c_set) {

    bool ns;
    signed long x, y;
    pair<signed long, signed long> p;
    // File.
    FILE* f = fopen(filename, "r");

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    char line[100];
    string s;
    while (fgets(line, 100, f)) {
        s = string(line);
        ns = false;
        x = 0;
        y = 0;

        for (const char &c: s) {
            switch (c) {
                case 'n':
                    y = y + 1;
                    ns = true;
                    break;
                case 's':
                    y = y - 1;
                    ns = true;
                    break;
                case 'e':
                    x += ns ? 1 : 2;
                    ns = false;
                    break;
                case 'w':
                    x -= ns ? 1 : 2;
                    ns = false;
                    break;
                default:
                    break;
            }
        }
        // cout << x << ',' << y << endl;
        p = {x, y};
        auto v = c_set.insert(p);
        if (!v.second) c_set.erase(p);
    }

    fclose(f);
    return c_set.size();
}

void insert(pair<signed long, signed long> &key, map<pair<signed long, signed long>, unsigned short> &m) {
    if (m.count(key)) {
        m[key] = m[key] + 1;
    }
    else {
        m[key] = 1;
    }
}

unsigned long solve_2(set<pair<signed long, signed long>> c_set) {
    set<pair<signed long, signed long>> to_white, to_black;
    map<pair<signed long, signed long>, unsigned short> m;
    unsigned long x, y, c;
    pair<signed long, signed long> p2;
    for (unsigned short i = 0; i < 100; ++i) {
        m.clear();
        to_white.clear();
        to_black.clear();

        for (const pair<signed long, signed long> &p : c_set) {
            c = 0;
            // e
            x = p.first + 2;
            y = p.second;
            p2 = { x , y };
            if (c_set.count(p2)) ++c;
            insert(p2, m);

            // ne
            x = p.first + 1;
            y = p.second + 1;
            p2 = { x , y };
            if (c_set.count(p2)) ++c;
            insert(p2, m);

            // se
            x = p.first + 1;
            y = p.second - 1;
            p2 = { x , y };
            if (c_set.count(p2)) ++c;
            insert(p2, m);

            // w
            x = p.first - 2;
            y = p.second;
            p2 = { x , y };
            if (c_set.count(p2)) ++c;
            insert(p2, m);

            // nw
            x = p.first - 1;
            y = p.second + 1;
            p2 = { x , y };
            if (c_set.count(p2)) ++c;
            insert(p2, m);

            // sw
            x = p.first - 1;
            y = p.second - 1;
            p2 = { x , y };
            if (c_set.count(p2)) ++c;
            insert(p2, m);

            if (c == 0 || c > 2) to_white.insert(p);
        }

        for (const auto &kv: m) {
            // Current tile is white (current tile not in black set).
            if (!c_set.count(kv.first)) {
                // Set to black.
                if (kv.second == 2) {
                    to_black.insert(kv.first);
                }
            }
        }

        for (const auto &tb : to_black) {
            c_set.insert(tb);
        }

        for (const auto &tw : to_white) {
            c_set.erase(tw);
        }
    }
    return c_set.size();
}

int main() {
    unsigned long n;
    set<pair<signed long, signed long>> c_set;

    // Part one.
    n = solve_1(INPUT_PATH, c_set);
    printf("Part 1: %lu\n", n);

    // Part 2.
    n = solve_2(c_set);
    printf("Part 2: %lu\n", n);

    return 0;
}
