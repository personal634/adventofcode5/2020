//
// Created by imanol on 2/12/20.
//

#include <iostream>
#include <utility>
#include <vector>
#include <string>
#include <unordered_map>
// #include <cstring>

using namespace std;

class Bag {
public:
    string color;
    vector<unsigned int> available_bags;
    vector<string> available_bag_colors;

    Bag();
    Bag(string c);
    void append(string c, unsigned int n);
    unsigned int bag_types() const;
};

Bag::Bag(string c) {
    this->color = move(c);
    // this->available_bag_colors = vector();
}

void Bag::append(string c, unsigned int n) {
    this->available_bag_colors.push_back(move(c));
    this->available_bags.push_back(n);
}

unsigned int Bag::bag_types() const {
    return this->available_bags.size();
}

Bag::Bag() {
    this->color = "";
}

const char* INPUT_PATH = "../input/input07";
const char* COLOR = "shiny gold";

void get_rules(const char* filename, unordered_map<string, Bag> &htmap) {
    FILE* f = fopen(filename, "r");
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    unsigned long n = 0, p, answers = 0;
    unsigned int p_c_end, p_c_start, p_cl_start;
    unsigned int p_n_end, p_listed_color_end;
    string color, l;
    Bag b;
    char line[500];

    while( fgets(line, 500, f) ) {
        l = string (line);
        // Skip to color end.
        p = l.find(' ');
        p = l.find(' ', p + 1);

        // Get color and init bag.
        color = l.substr(0, p);
        b = Bag(color);

        // Skip to contain end.
        p = l.find(' ', p + 1);
        p = l.find(' ', p + 1);

        while (p < l.size()) {
            // Skip to number end, but keep current position.
            n = p + 1;
            p = l.find(' ', p + 1);

            // Get number.
            if(l.at(p - 1) == 'o') break;
            else {
                n = stoi(l.substr(n, p - n));
            }

            // Skip to color end, keep current position.
            p_c_start = p + 1;
            p = l.find(' ', p + 1);
            p = l.find(' ', p + 1);

            // Get color.
            color = l.substr(p_c_start, p - p_c_start);

            // Set contained bag.
            b.append(color, n);

            // Next color or line end.
            p = l.find(' ', p + 1);
        }

        // htmap.operator[](b.color) = b;
        htmap[b.color] = b;
    }
    fclose(f);
}

unsigned long count_bag_containers(const string& c, const Bag& b, unordered_map<string, Bag> &htmap) {
    unsigned long n = 0;

    if (b.color == c) return 0;
    // cout << b.bag_types() << endl;

    if (b.bag_types() == 0) {
        return 0;
    }
    else {
        for (int i = 0; i < b.bag_types(); ++i) {
            string bc (b.available_bag_colors.at(i));
            unsigned int bn = b.available_bags.at(i);
            // cout << "|" << bc << "|" << endl;

            if (c == bc) n += bn;
            n += count_bag_containers(c, htmap[bc], htmap);
        }
        return n;
    }

    return n;
}

unsigned long count_contained_bags(const string& c, unordered_map<string, Bag> &htmap) {
    unsigned long n = 1;

    for (int i = 0; i < htmap[c].bag_types(); ++i) {
        string bc (htmap[c].available_bag_colors.at(i));
        unsigned int bn = htmap[c].available_bags.at(i);

        n += bn * count_contained_bags(bc, htmap);
    }

    return n;
}

int main() {
    unordered_map<string, Bag> map;
    unsigned long n = 0;

    // Part one.
    get_rules(INPUT_PATH, map);
    for (pair<string, Bag> p : map) {
        n += count_bag_containers(COLOR, p.second, map) > 0 ? 1 : 0;
        // cout << p.first << endl;
    }

    printf("Part 1: %lu\n", n);

    // Part two.
    n = count_contained_bags(COLOR, map) - 1;
    printf("Part 2: %lu\n", n);

    return 0;
}