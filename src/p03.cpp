//
// Created by imanol on 2/12/20.
//

#include <cstring>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

const char* INPUT_PATH = "../input/input03";

unsigned long count_trees(const char* filename, const unsigned int dx, const unsigned int dy) {
    FILE* f = fopen(filename, "r");
    if (f == NULL) {
        printf("Error loading file.");
        exit(1);
    }
    unsigned long counter = 0;
    unsigned short line_length;
    long pos, x = 0, y = 0;
    char val_str[100];
    int c;
    fgets(val_str, 100, f);
    string s (val_str);
    line_length = s.size();
    // cout << line_length << endl;
    fseek(f, 0, SEEK_SET);

    c = fgetc(f);
    while(c != EOF) {
        // cout << (char) c << endl;
        if (c == '#') counter += 1;
        x = (x + dx) % (line_length - 1);
        y = y + dy;
        pos = x + line_length * y;
        fseek(f, pos, SEEK_SET);
        c = fgetc(f);
    }
    fclose(f);
    return counter;
}

int main() {
    unsigned long n, dx = 3, dy = 1;

    // Part one.
    n = count_trees(INPUT_PATH, dx, dy);
    printf("Part 1: %lu\n", n);

    // Part two.
    n = 1;
    n *= count_trees(INPUT_PATH, 1, 1);
    n *= count_trees(INPUT_PATH, 3, 1);
    n *= count_trees(INPUT_PATH, 5, 1);
    n *= count_trees(INPUT_PATH, 7, 1);
    n *= count_trees(INPUT_PATH, 1, 2);
    printf("Part 2: %lu\n", n);

    return 0;
}