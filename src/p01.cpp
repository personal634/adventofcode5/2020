//
// Created by imanol on 2/12/20.
//

#include <cstring>
#include <vector>
#include <iostream>
using namespace std;

const char* INPUT_PATH = "../input/input01";

void get_input(const char* filename, vector<unsigned int>* v) {
    FILE* f = fopen(filename, "r");
    if (f == NULL) {
        printf("Error loading file.");
        return;
    }
    // fseek(f, 0, SEEK_SET);
    unsigned int val = 0;
    char val_str[100];
    while(fgets(val_str, 100, f) != NULL) {
        val = atoi(val_str);
        v->push_back(val);
    }
    fclose(f);

}

int main() {
    // Get input.
    vector<unsigned int> v;
    get_input(INPUT_PATH, &v);
    unsigned int n = v.size();

    // Part one.
    for (unsigned int i = 0; i < n-1; ++i) {
        for (unsigned int j = i+1; j < n; ++j) {
            if (v.at(i) + v.at(j) == 2020) {
                printf("Part 1: %d\n", v.at(i) * v.at(j));
                break;
            }
        }
    }

    // Part two.
    for (unsigned int i = 0; i < n-2; ++i) {
        for (unsigned int j = i+1; j < n-1; ++j) {
            for (unsigned int k = j+1; k < n; ++k) {
                if (v.at(i) + v.at(j) + v.at(k) == 2020) {
                    printf("Part 2: %d\n", v.at(i) * v.at(j) * v.at(k));
                    return 0;
                }
            }
        }
    }


    return 1;
}