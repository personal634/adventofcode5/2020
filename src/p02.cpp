//
// Created by imanol on 2/12/20.
//

#include <cstring>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

const char* INPUT_PATH = "../input/input02";

struct Entry {
    char character;
    unsigned short lower_bound;
    unsigned short upper_bound;
    char* password;
};

unsigned int get_valid_old_policy(const char* filename) {
    FILE* f = fopen(filename, "r");
    if (f == NULL) {
        printf("Error loading file.");
        exit(1);
    }
    unsigned int counter = 0, n;
    unsigned short lb, ub, pm, pc;
    char val_str[100], c;
    while(fgets(val_str, 100, f) != NULL) {
        string s (val_str);
        pm = s.find("-");
        pc = s.find(":");
        lb = stoi(s.substr(0, pm));
        ub = stoi(s.substr(pm + 1, pc - 3 - pm));
        c = s.at(pc - 1);

        string substr = s.substr(pc + 2, 100);
        n = count(substr.begin(), substr.end(), c);

        if (lb <= n && n <= ub) counter += 1;

        // cout << lb << "-" << ub << " " << c << ": " << substr << endl;
    }
    fclose(f);
    return counter;
}

unsigned int get_valid_new_policy(const char* filename) {
    FILE* f = fopen(filename, "r");
    if (f == NULL) {
        printf("Error loading file.");
        exit(1);
    }
    unsigned int counter = 0;
    unsigned short lb, ub, pm, pc;
    char val_str[100], c;
    while(fgets(val_str, 100, f) != NULL) {
        string s (val_str);
        pm = s.find("-");
        pc = s.find(":");
        lb = stoi(s.substr(0, pm));
        ub = stoi(s.substr(pm + 1, pc - 3 - pm));
        c = s.at(pc - 1);

        string substr = s.substr(pc + 2, 100);

        if (substr.at(lb-1) == c xor substr.at(ub-1) == c) counter += 1;

        // cout << c << " " << substr.at(lb - 1) << substr.at(ub - 1) << endl;
    }
    fclose(f);
    return counter;
}

int main() {
    unsigned int n;

    // Part one.
    n = get_valid_old_policy(INPUT_PATH);
    printf("Part 1: %d\n", n);

    // Part two.
    n = get_valid_new_policy(INPUT_PATH);
    printf("Part 2: %d\n", n);

    return 0;
}