//
// Created by imanol on 2/12/20.
//

#include <iostream>
#include <vector>

using namespace std;

const char* INPUT_PATH = "../input/input17";

void print_puzzle(const vector<bool> *map, const unsigned long *nx, const unsigned long *ny, const unsigned long *nz) {
    cout << "---------------" << endl;
    for (unsigned long k = 0ul; k < *nz; ++k) {
        for (unsigned long j = 0ul; j < *ny; ++j) {
            for (unsigned long i = 0ul; i < *nx; ++i) {
                cout << (map->at(k*(*nx)*(*ny)+j*(*nx)+i) ? '#' : '.');
            }
            cout << endl;
        }
        cout << endl;
    }
    cout << "---------------" << endl;
}

void get_input(const char* filename, vector<bool> *map, unsigned long *nx, unsigned long *ny, unsigned long *nz) {
    // File.
    FILE* f = fopen(filename, "r");
    unsigned int p_colon, p_or, p_minus_1, p_minus_2;

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    char line[100];
    string s;
    *nx = 0;
    *ny = 0;
    *nz = 1;

    while (fgets(line, 100, f)) {
        s = string (line);
        *nx = 0;
        *ny = s.find('\n');

        for (char c : line) {
            if (c == '#') {
                map->push_back(true);
                *nx += 1;
            }
            else if (c == '.') {
                map->push_back(false);
                *nx += 1;
            }
        }
    }

    fclose(f);
}

unsigned long count_active(const vector<bool> *map) {
    unsigned long n = 0ul;
    for (bool s : *map) n += s ? 1ul : 0ul;
    return n;
}

unsigned long count_boundary_active(const vector<bool> *map, const unsigned long *nx, const unsigned long *ny, const unsigned long *nz) {
    unsigned long n = 0ul, l, lp, i, j, k;
    const unsigned long m = (*nx) * (*ny) * (*nz) - (*nx-2) * (*ny-2) * (*nz-2);

    for (l = 0ul; l < m; ++l) {

        // i = 0 plane.
        if (l < (*ny) * (*nz)) {
            i = 0ul;
            j = l / (*nz);
            k = l % (*nz);
        }
        // i = nx - 1 plane.
        else if (l < 2ul * (*ny) * (*nz)) {
            lp = l - (*ny) * (*nz);
            i = (*nx) - 1ul;
            j = lp / (*nz);
            k = lp % (*nz);
        }
        // k = 0 plane (i = 0 and i = nx - 1 not included).
        else if (l < 2ul*(*ny)*(*nz) + (*nx-2ul)*(*ny)) {
            lp = l - 2ul*(*ny)*(*nz);
            i = 1ul + (lp % (*nx - 2ul));
            j = lp / (*nx - 2ul);
            k = 0ul;
        }
        // k = nz - 1 plane (i = 0 and i = nx - 1 not included).
        else if (l < 2ul*(*ny)*(*nz) + 2ul*(*nx-2ul)*(*ny)) {
            lp = l - 2ul*(*ny)*(*nz) - (*nx-2ul)*(*ny);
            i = 1ul + (lp % (*nx - 2ul));
            j = lp / (*nx - 2ul);
            k = *nz - 1ul;
        }
        // j = 0 plane (only inner points).
        else if (l < 2ul*(*ny)*(*nz) + 2ul*(*nx-2ul)*(*ny) + (*nx-2ul)*(*nz-2ul)) {
            lp = l - 2ul*(*ny)*(*nz) - 2ul*(*nx-2ul)*(*ny);
            i = 1ul + lp / (*nz - 2ul);
            j = 0ul;
            k = 1ul + (lp % (*nz - 2ul));
        }
        else {
            lp = l - 2ul*(*ny)*(*nz) - 2ul*(*nx-2ul)*(*ny) - (*nx-2ul)*(*nz-2ul);
            i = 1ul + lp / (*nz - 2ul);
            j = *ny - 1ul;
            k = 1ul + (lp % (*nz - 2ul));
        }

        n += map->at(k * (*ny) * (*nx) + j * (*nx) + i) ? 1ul : 0ul;
    }

    return n;
}

unsigned long count_boundary_active_4d(const vector<bool> *map, const unsigned long *nx, const unsigned long *ny, const unsigned long *nz, const unsigned long *nw) {

    return 1ul;
}

void increase_space(vector<bool> *map, unsigned long *nx, unsigned long *ny, unsigned long *nz) {
    unsigned long i, j, k;
    vector<bool> v2 ((*nx+2)*(*ny+2)*(*nz+2), false);
    for (i = 0ul; i < *nx; ++i) {
        for (j = 0ul; j < *ny; ++j) {
            for (k = 0ul; k < *nz; ++k) {
                v2[(k+1ul)*(*ny+2ul)*(*nx+2ul)+(j+1ul)*(*nx+2ul)+(i+1ul)] = map->at(k*(*ny)*(*nx)+j*(*nx)+i);
            }
        }
    }
    *nx = *nx + 2ul;
    *ny = *ny + 2ul;
    *nz = *nz + 2ul;
    swap(*map, v2);
    //for (l = 0ul; l < (*nx) * (*ny); ++l) map->insert(map->begin(), false);
    // for (l = 0ul; l < (*nx) * (*ny); ++l) map->push_back(false);
    // *nz = *nz + 2ul;
}

void increase_space_4d(vector<bool> *map, unsigned long *nx, unsigned long *ny, unsigned long *nz, unsigned long *nw) {
    unsigned long i, j, k, l;
    vector<bool> v2 ((*nx+2)*(*ny+2)*(*nz+2)*(*nw+2), false);
    for (i = 0ul; i < *nx; ++i) {
        for (j = 0ul; j < *ny; ++j) {
            for (k = 0ul; k < *nz; ++k) {
                for (l = 0ul; l < *nw; ++l) {
                    v2[(l+1ul)*(*nz+2ul)*(*ny+2ul)*(*nx+2ul)+(k+1ul)*(*ny+2ul)*(*nx+2ul)+(j+1ul)*(*nx+2ul)+(i+1ul)] = map->at(l*(*nz)*(*ny)*(*nx)+k*(*ny)*(*nx)+j*(*nx)+i);
                }
            }
        }
    }
    *nx = *nx + 2ul;
    *ny = *ny + 2ul;
    *nz = *nz + 2ul;
    *nw = *nw + 2ul;
    swap(*map, v2);
}

unsigned int count_nearby(const vector<bool> *map, const unsigned long *l, const unsigned long *nx, const unsigned long *ny, const unsigned long *nz) {
    unsigned int n = 0;
    signed long i, j, k;
    signed long long dx, dy, dz;
    unsigned long lp, lc;

    for (i = 0; i < 3; ++i) {
        for (j = 0; j < 3; ++j) {
            for (k = 0; k < 3; ++k) {
                lp = *l - (*l / *nx / *ny) * *nx * *ny;
                lc = lp % *nx;
                if (i == 1 && j == 1 && k == 1) continue;
                if (k == 2 && *l / (*nx) / (*ny) == *nz - 1) continue;
                if (k == 0 && *l / (*nx) / (*ny) == 0) continue;
                if (j == 2 && lp / *nx == *ny - 1) continue;
                if (j == 0 && lp / *nx == 0) continue;
                if (i == 2 && lc == *nx - 1) continue;
                if (i == 0 && lc == 0) continue;
                dx = (signed long long) i - 1ll;
                dy = ((signed long long) j - 1ll) * ((signed long long) *nx);
                dz = ((signed long long) k - 1ll) * ((signed long long) *nx) * ((signed long long) *ny);
                n += map->at((signed long long) *l + dz + dy + dx) ? 1 : 0;
            }
        }
    }
    /*
    for (m = 0; m < 8; ++m) {
        // cout << (1-2*(m & 1));
        // cout << (1-2*((m >> 1) & 1));
        // cout << (1-2*((m >> 2) & 1));
        n += map->at((signed long long) *l + (1-2*(m & 1))*(*nx)*(*ny) + (1-2*((m >> 1) & 1))*(*nx) + (1-2*((m >> 2) & 1))) ? 1 : 0;
    }
    */

    return n;
}

unsigned int count_nearby_4d(const vector<bool> *map, const unsigned long *n, const unsigned long *nx, const unsigned long *ny, const unsigned long *nz, const unsigned long *nw) {
    unsigned int m = 0;
    signed long i, j, k, l;
    signed long long dx, dy, dz,dw;
    unsigned long ls, lp, lc;

    for (i = 0; i < 3; ++i) {
        for (j = 0; j < 3; ++j) {
            for (k = 0; k < 3; ++k) {
                for (l = 0; l < 3; ++l) {
                    // Define position on lower dimensions.
                    // Get position on 3D subspace.
                    ls = *n - (*n / *nx / *ny / *nz) * *nx * *ny * *nz;
                    // Get position on 2D plane.
                    lp = ls - (ls / *nx / *ny) * *nx * *ny;
                    // Get position on 1D axis.
                    lc = lp % *nx;

                    // Skip evaluated position.
                    if (i == 1 && j == 1 && k == 1 && l == 1) continue;
                    // Skip out-of-bounds.
                    if (l == 2 && *n / (*nx) / (*ny) / (*nz) == *nw - 1) continue;
                    if (l == 0 && *n / (*nx) / (*ny) / (*nz) == 0) continue;
                    if (k == 2 && ls / (*nx) / (*ny) == *nz - 1) continue;
                    if (k == 0 && ls / (*nx) / (*ny) == 0) continue;
                    if (j == 2 && lp / *nx == *ny - 1) continue;
                    if (j == 0 && lp / *nx == 0) continue;
                    if (i == 2 && lc == *nx - 1) continue;
                    if (i == 0 && lc == 0) continue;

                    // Get nearby position.
                    dx = (signed long long) i - 1ll;
                    dy = ((signed long long) j - 1ll) * ((signed long long) *nx);
                    dz = ((signed long long) k - 1ll) * ((signed long long) *nx) * ((signed long long) *ny);
                    dw = ((signed long long) l - 1ll) * ((signed long long) *nx) * ((signed long long) *ny) * ((signed long long) *nz);
                    m += map->at((signed long long) *n + dw + dz + dy + dx) ? 1 : 0;
                }

            }
        }
    }

    return m;
}

unsigned long solve_1(vector<bool> *map, unsigned long *nx, unsigned long *ny, unsigned long *nz, const unsigned long c) {
    unsigned long counter = 0;
    unsigned long i, j, k, l;
    unsigned short m;
    vector<bool> map_temp;

    while (counter < c) {
        // print_puzzle(map, nx, ny, nz);
        if (*nx < 3 || *ny < 3 || *nz < 3) {
            increase_space(map, nx, ny, nz);
        }

        if (count_boundary_active(map, nx, ny, nz) > 0) increase_space(map, nx, ny, nz);
        map_temp = vector<bool> ((*nx)*(*ny)*(*nz));

        for (k = 0; k < *nz; ++k) {
            for (j = 0; j < *ny; ++j) {
                for (i = 0; i < *nx; ++i) {
                    l = k*(*nx)*(*ny)+j*(*nx)+i;
                    m = count_nearby(map, &l, nx, ny, nz);

                    if (map->at(l)) {
                        map_temp[l] = m >= 2 && m <= 3;
                    }
                    else {
                        map_temp[l] = m == 3;
                    }
                }
            }
        }
        swap(*map, map_temp);
        // print_puzzle(map, nx, ny, nz);
        counter += 1;
    }

    return count_active(map);
}

unsigned long solve_2(vector<bool> *map, unsigned long *nx, unsigned long *ny, unsigned long *nz, unsigned long *nw, const unsigned long c) {
    unsigned long counter = 0;
    unsigned long i, j, k, l, n;
    unsigned short m;
    vector<bool> map_temp;

    while (counter < c) {
        if (*nx < 3 || *ny < 3 || *nz < 3 || *nw < 3) {
            increase_space_4d(map, nx, ny, nz, nw);
        }

        if (count_boundary_active_4d(map, nx, ny, nz, nw) > 0) increase_space_4d(map, nx, ny, nz, nw);
        map_temp = vector<bool> ((*nx)*(*ny)*(*nz)*(*nw));

        for (l = 0; l < *nw; ++l) {
            for (k = 0; k < *nz; ++k) {
                for (j = 0; j < *ny; ++j) {
                    for (i = 0; i < *nx; ++i) {
                        n = l*(*nx)*(*ny)*(*nz)+k*(*nx)*(*ny)+j*(*nx)+i;
                        m = count_nearby_4d(map, &n, nx, ny, nz, nw);

                        if (map->at(n)) {
                            map_temp[n] = m >= 2 && m <= 3;
                        }
                        else {
                            map_temp[n] = m == 3;
                        }
                    }
                }
            }
        }
        swap(*map, map_temp);
        counter += 1;
    }

    return count_active(map);
}

int main() {
    unsigned long n, nx, ny, nz, nw = 1ul;
    vector<bool> map;

    // Part one.
    get_input(INPUT_PATH, &map, &nx, &ny, &nz);
    // cout << "NX: " << nx << " , NY: " << ny << ", NZ: " << nz << endl;
    // n = count_active(&map);
    n = solve_1(&map, &nx, &ny, &nz, 6);
    printf("Part 1: %lu\n", n);

    // Part two.
    map = vector<bool> ();
    get_input(INPUT_PATH, &map, &nx, &ny, &nz);
    n = solve_2(&map, &nx, &ny, &nz, &nw, 6);
    printf("Part 2: %lu\n", n);

    return 0;
}
