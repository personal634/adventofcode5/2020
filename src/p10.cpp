//
// Created by imanol on 2/12/20.
//

#include <iostream>
#include <vector>

using namespace std;

const char* INPUT_PATH = "../input/input10";

void bubble_sort(vector<unsigned int> *v) {
    unsigned long n = v->size();
    unsigned short swapped;
    unsigned long i;
    unsigned int temp;

    do {
        swapped = 0;
        for(i = 1; i < n; ++i) {
            if (v->at(i-1) > v->at(i)) {
                temp = v->at(i-1);
                v->operator[](i-1) = v->at(i);
                v->operator[](i)= temp;
                swapped = 1;
            }
        }
    } while (swapped == 1);
}

void get_adapters(const char* filename, vector<unsigned int> *v) {
    FILE* f = fopen(filename, "r");
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    unsigned long c_max = 0, c;
    char line[500];

    // Add seat 0 jolts.
    v->push_back(0);

    // Add adapters.
    while ( fgets(line, 500, f) ) {
        c = stoul(line);
        c_max = c > c_max ? c : c_max;
        v->push_back(c);
    }

    // Add device jolts.
    v->push_back(c_max + 3);
}

unsigned long solve_1(vector<unsigned int> *v) {
    unsigned long d1 = 0, d3 = 0, df;
    unsigned long i;

    for (i = 1; i < v->size(); ++i) {
        df = v->at(i) - v->at(i-1);
        if (df == 1) d1 += 1;
        if (df == 3) d3 += 1;
    }
    return d1 * d3;
}

unsigned long back_traverse(unsigned long p0, vector<vector<unsigned long>> *paths) {
    unsigned long n = 0;
    vector<unsigned long> bifurcation = paths->at(p0);
    unsigned short i, m = bifurcation.size();

    if (m == 0) {
        return 1;
    }
    //else if (m == 1) {
    //    return back_traverse(bifurcation.at(0), paths);
    //}
    else {
        for (i = 0; i < m; ++i) {
            n += back_traverse(bifurcation.at(i), paths);
        }
    }
    return n;
}

unsigned long solve_2(vector<unsigned int> *v) {
    unsigned long i, j, m = v->size();
    unsigned long c_val;

    vector<vector<unsigned long>> paths;
    vector<unsigned long> values;

    for (i = m - 1; i < m; --i) {
        c_val = v->at(i);
        vector<unsigned long> sp;
        values.push_back(0);

        if (i == 0)
            paths.push_back(sp);
        else if ( i == 1 ) {
            // sp.push_back(0);
            for (j = 1; j <= 1; ++j) {
                if (c_val - v->at(i-j) <= 3) sp.push_back(m-1-(i-j));
            }
            paths.push_back(sp);
        }
        else if ( i == 2 ) {
            // if (c_val - v->at(1) <= 3) sp.push_back(1);
            // if (c_val - v->at(0) <= 3) sp.push_back(0);
            for (j = 1; j <= 2; ++j) {
                if (c_val - v->at(i-j) <= 3) sp.push_back(m-1-(i-j));
            }
            paths.push_back(sp);
        }
        else {
            for (j = 1; j <= 3; ++j) {
                if (c_val - v->at(i-j) <= 3) sp.push_back(m-1-(i-j));
            }
            paths.push_back(sp);
        }
    }

    values[values.size()-1] = 1;

    for (i = values.size() - 1; i < values.size(); --i) {
        c_val = 0;

        for (unsigned long sp : paths.at(i)) {
            c_val += values.at(sp);
        }
        if (c_val == 0) c_val = 1;

        values[i] = c_val;
    }

    return values[0];
    // return back_traverse(0, &paths);
}

int main() {
    unsigned long n;
    vector<unsigned int> adapters;

    // Part one.
    get_adapters(INPUT_PATH, &adapters);
    bubble_sort(&adapters);
    n = solve_1(&adapters);
    printf("Part 1: %lu\n", n);

    // Part two.
    n = solve_2(&adapters);
    printf("Part 2: %lu\n", n);

    return 0;
}
