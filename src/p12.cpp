//
// Created by imanol on 2/12/20.
//

#include <iostream>

using namespace std;

const char* INPUT_PATH = "../input/input12";

unsigned long norm0(const long i0, const long j0, const long in, const long jn) {
    return abs(in - i0) + abs(jn - j0);
}

unsigned long solve_1(const char* filename) {
    // Initial position.
    signed long x = 0, y = 0;
    // Initial front direction.
    signed short dir_x = 1, dir_y = 0;
    signed int c_angle = 0;
    // Instruction vars.
    char line[500];
    char command;
    signed int value;
    // File.
    FILE* f = fopen(filename, "r");

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    while ( fgets(line, 500, f) ) {
        if (line[0] == '\n') continue;

        command = line[0];
        value = stoi(line+1);

        if (command == 'N') {
            y += value;
        }
        else if (command == 'S') {
            y -= value;
        }
        else if (command == 'E') {
            x += value;
        }
        else if (command == 'W') {
            x -= value;
        }
        else if (command == 'L' || command == 'R') {
            if (command == 'L') {
                c_angle += value;
            }
            else {
                c_angle -= value;
            }

            while (c_angle < 0) c_angle += 360;
            c_angle = c_angle % 360;
            switch (c_angle) {
                case 0:
                    dir_x = 1;
                    dir_y = 0;
                    break;
                case 90:
                    dir_x = 0;
                    dir_y = 1;
                    break;
                case 180:
                    dir_x = -1;
                    dir_y = 0;
                    break;
                case 270:
                    dir_x = 0;
                    dir_y = -1;
                    break;
                default:
                    cout << "Invalid angle: " << c_angle << endl;
                    break;
            }

            }
        else if (command == 'F') {
            x += value * dir_x;
            y += value * dir_y;
        }
    }

    fclose(f);
    return norm0(0, 0, x, y);
}

unsigned long solve_2(const char* filename) {
    // Initial position.
    signed long p_x = 0, p_y = 0;
    // Waypoint initial position.
    signed long w_x = 10, w_y = 1;
    signed long temp_x, temp_y;
    // Nx90 degrees.
    unsigned int n;
    // Instruction vars.
    char line[500];
    char command;
    signed int value;
    // File.
    FILE* f = fopen(filename, "r");

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    while ( fgets(line, 500, f) ) {
        if (line[0] == '\n') continue;

        command = line[0];
        value = stoi(line+1);

        if (command == 'N') {
            w_y += value;
        }
        else if (command == 'S') {
            w_y -= value;
        }
        else if (command == 'E') {
            w_x += value;
        }
        else if (command == 'W') {
            w_x -= value;
        }
        else if (command == 'L' || command == 'R') {
            n = (value % 360) / 90;

            if (n % 2 == 0) {
                temp_x = w_x;
                temp_y = w_y;
            }
            else {
                temp_x = w_y;
                temp_y = w_x;
            }

            if (command == 'L') {
                if (n == 1 || n == 2) temp_x = -temp_x;
                if (n == 2 || n == 3) temp_y = -temp_y;
            }
            else {
                if (n == 2 || n == 3) temp_x = -temp_x;
                if (n == 1 || n == 2) temp_y = -temp_y;
            }

            w_x = temp_x;
            w_y = temp_y;
        }
        else if (command == 'F') {
            p_x += value * w_x;
            p_y += value * w_y;
        }
    }

    fclose(f);
    return norm0(0, 0, p_x, p_y);
}

int main() {
    unsigned long n;

    // Part one.
    n = solve_1(INPUT_PATH);
    printf("Part 1: %lu\n", n);

    // Part two.
    n = solve_2(INPUT_PATH);
    printf("Part 2: %lu\n", n);

    return 0;
}
