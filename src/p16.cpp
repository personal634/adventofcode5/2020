//
// Created by imanol on 2/12/20.
//

#include <iostream>
#include <random>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <regex>

using namespace std;

const char* INPUT_PATH = "../input/input16";
const char* REGEX_FIELD = R"([\w\s]+:\s\d+[-]\d+\sor\s\d+[-]\d+\s*)";

namespace solution_1 {

    unsigned long ticket_error_rate(const vector<vector<unsigned int>> *ranges, const vector<unsigned int> *ticket) {
        unsigned int c;
        unsigned short r1, r2;
        unsigned long n = 0;

        // check each field.
        for (unsigned int num : *ticket) {
            // for (vector<unsigned int> f : *ranges) {
            c = 0;

            for (vector<unsigned int> f : *ranges) {
                // for (unsigned int num : *ticket) {
                r1 = (f.at(0) <= num && num <= f.at(1)) ? 1 : 0;
                r2 = (f.at(2) <= num && num <= f.at(3)) ? 1 : 0;
                c += r1 | r2;
            }
            // cout << c << endl;

            // cout << c << ":" << num << endl;

            if (c == 0) n += num;
        }

        return n;
    }

    unsigned long count_error_rate(const vector<vector<unsigned int>> *ranges, const vector<vector<unsigned int>> *tickets) {
        unsigned long n = 0;

        // Iter over tickets.
        for (const vector<unsigned int>& t : *tickets) {
            n += ticket_error_rate(ranges, &t);
        }

        return n;
    }

}

namespace solution_2 {
    bool check_range(const unsigned int *num, const vector<unsigned int> *ranges) {
        //! Check if ticket field matches provided range.

        return (ranges->at(0) <= *num && *num <= ranges->at(1)) || (ranges->at(2) <= *num && *num <= ranges->at(3));
    }

    unsigned long check_range_on_field(vector<vector<unsigned int>> *tickets, unsigned int ticket_idx, vector<unsigned int> *ranges) {
        //! Get the count of tickets whose *ticket_idx* fits the provided range.

        unsigned long n = 0;
        for (vector<unsigned int> ticket : *tickets) {
            unsigned int num = ticket.at(ticket_idx);
            n += check_range(&num, ranges) ? 1 : 0;
        }
        return n;
    }

    unsigned long count_in_vector(vector<unsigned int> *v, unsigned int j) {
        //! Count value matches on a vector.
        unsigned long n = 0;
        for (unsigned int num : *v) if (num == j) n += 1;
        return n;
    }

    unsigned short ticket_is_correct(const vector<vector<unsigned int>> *ranges, const vector<unsigned int> *ticket) {
        //! Check that *ticket* has at least one valid range for all of its fields.

        unsigned int c;
        unsigned short r1, r2;

        // check each field.
        for (unsigned int num : *ticket) {
            // for (vector<unsigned int> f : *ranges) {
            c = 0;

            for (vector<unsigned int> f : *ranges) {
                // for (unsigned int num : *ticket) {
                r1 = (f.at(0) <= num && num <= f.at(1)) ? 1 : 0;
                r2 = (f.at(2) <= num && num <= f.at(3)) ? 1 : 0;
                c += r1 | r2;
            }
            if (c == 0) {
                return 0;
            }
        }
        return 1;
    }

    void get_only_correct(vector<vector<unsigned int>> *ranges, vector<vector<unsigned int>> *tickets, vector<vector<unsigned int>> *new_tickets) {
        //! Fill *new_tickets* with the valid tickets subset of initial tickets.

        for (const vector<unsigned int>& ticket : *tickets) {
            if (ticket_is_correct(ranges, &ticket) > 0) {
                new_tickets->push_back(ticket);
            }
        }
    }

    void sort_by_guess(vector<vector<unsigned int>> *guesses, vector<unsigned int> *idxs) {
        unsigned long n = guesses->size();
        unsigned short swapped;
        unsigned long i;
        vector<unsigned int> temp;
        unsigned int temp_i;

        do {
            swapped = 0;
            for(i = 1; i < n; ++i) {
                if (guesses->at(i-1).size() > guesses->at(i).size()) {
                    temp = guesses->at(i-1);
                    temp_i = idxs->at(i-1);
                    guesses->operator[](i-1) = guesses->at(i);
                    guesses->operator[](i) = temp;
                    idxs->operator[](i-1) = idxs->at(i);
                    idxs->operator[](i) = temp_i;
                    swapped = 1;
                }
            }
        } while (swapped == 1);
    }

    void discard_repeated(vector<vector<unsigned int>> *guesses, unsigned int *start) {
        unsigned int temp, j, k;
        unsigned int swapped = 0;

        if (guesses->at(*start).size() == 1) {
            temp = guesses->at(*start).at(0);
            *start = *start + 1;
            swapped = 1;

            for (j = *start; j < guesses->size(); ++j) {
                vector<unsigned int> v = guesses->at(j);
                vector<unsigned int> *p_v = &guesses->at(j);
                for (k = v.size()-1; k < v.size(); --k) {
                    if (v.at(k) == temp) {
                        p_v->erase(p_v->begin()+k);
                    }
                }
            }
        }

        if (swapped > 0 && *start < guesses->size()) discard_repeated(guesses, start);
    }

    void invert_idxs(vector<unsigned int> *in, vector<unsigned int> *out) {
        //! Invert index vector.
        unsigned int v_min, i_min, c_idx = 0, k;

        while (out->size() < in->size()) {
            for (k = 0; k < in->size(); ++k) {
                if (in->at(k) == c_idx) out->push_back(k);
            }
            c_idx += 1;
        }
    }

    unsigned long solve_2(vector<vector<unsigned int>> *ranges, vector<string> *fields, vector<vector<unsigned int>> *tickets, vector<unsigned int> *my_ticket) {
        // Store correct tickets.
        vector<vector<unsigned int>> correct_tickets;
        // 0 to N indices.
        vector<unsigned int> idxs, idxs_rev;
        // 0 to N indices.
        vector<vector<unsigned int>> shuffled_ranges(fields->size());
        // Return value.
        unsigned long long n;
        // Count of valid tickets.
        unsigned long n_valid_tickets;
        // Field number.
        unsigned int n_fields = fields->size();
        // Possible field indices of a field.
        vector<vector<unsigned int>> fields_guesses;
        // Iterator.
        unsigned int i, j;

        // Discard bad tickets.
        get_only_correct(ranges, tickets, &correct_tickets);
        n_valid_tickets = correct_tickets.size();

        // Apply field ranges to all fields and store indices
        //     which make all ticket fields valid.
        for (i = 0; i < n_fields; ++i) {
            vector<unsigned int> field_guesses;
            for (j = 0; j < n_fields; ++j) {
                if (check_range_on_field(&correct_tickets, j, &ranges->at(i)) == n_valid_tickets) field_guesses.push_back(j);
            }
            fields_guesses.push_back(field_guesses);
        }

        // Each fields_guesses index relates to its ordered field value,
        //     and its value, the valid field order it can fit.

        // Sort fields by guesses. The field with less available guesses
        // comes first (one guess would mean correct answer).
        // A vector of original indices to new indices is required.
        for (i = 0; i < n_fields; ++i) idxs.push_back(i);
        sort_by_guess(&fields_guesses, &idxs);
        invert_idxs(&idxs, &idxs_rev);

        // Check that field indices are correct:
        // Each fields_guesses index relates to its a field value,
        //     and its value, the valid field order it can fit.
        // Each idxs index relates to its ordered field value,
        //     and its value, to its fields_guesses index.

        // If a lonely guess appears, delete any other matches.
        i = 0;
        discard_repeated(&fields_guesses, &i);

        // All guesses now are unique. Set new field indices.
        for (i = 0; i < n_fields; ++i) {
            idxs[i] = fields_guesses.at(idxs_rev.at(i)).at(0);
        }

        // Get the wanted result. Correct field positions are stored at idxs.
        n = 1;
        for (i = 0; i < fields->size(); ++i) {
            if (fields->at(i).find("departure") < fields->at(i).size()) {
                n *= my_ticket->operator[](idxs.at(i));
            }
        }
        return n;
    }
}

void get_ticket(const char* line, vector<unsigned int> *t) {
    unsigned int p0 = 0, pc;
    string s (line);

    do {
        pc = s.find(',', p0 + 1);
        t->push_back(stoi(s.substr(p0, pc - p0)));
        p0 = pc + 1;
    } while (pc < s.size());
}

void get_input(const char* filename, vector<vector<unsigned int>> *ranges, vector<string> *fields, vector<vector<unsigned int>> *tickets, vector<unsigned int> *my_ticket) {
    // File.
    FILE* f = fopen(filename, "r");
    unsigned int p_colon, p_or, p_minus_1, p_minus_2;

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    char line[100];
    string s;

    while (fgets(line, 100, f)) {
        s = string (line);

        if ( regex_match(line, regex(REGEX_FIELD)) ) {
            // cout << "Field found." << endl;

            p_colon = s.find(':');
            p_or = s.find(" or ");
            p_minus_1 = s.find('-', p_colon);
            p_minus_2 = s.find('-', p_or);

            fields->push_back(s.substr(0, p_colon));

            vector<unsigned int> field;
            field.push_back(stoi(s.substr(p_colon+2, p_colon+2-p_minus_1)));
            field.push_back(stoi(s.substr(p_minus_1+1, p_minus_1+1-p_or)));
            field.push_back(stoi(s.substr(p_or+4, p_or+4-p_minus_2)));
            field.push_back(stoi(s.substr(p_minus_2+1)));
            ranges->push_back(field);
        }
        else if ( regex_match(line, regex(R"(\s*your ticket:\s*)")) ) {
            // cout << "My ticket found." << endl;
            fgets(line, 100, f);
            get_ticket(line, my_ticket);
        }
        else if ( regex_match(line, regex(R"(\s*nearby tickets:\s*)")) ) {
            // cout << "Nearby tickets found." << endl;
            while (fgets(line, 100, f)) {
                vector<unsigned int> ticket;
                get_ticket(line, &ticket);
                tickets->push_back(ticket);
            }
        }
    }

    fclose(f);
}

/*
unsigned long count_incorrect(const vector<vector<unsigned int>> *ranges, const vector<vector<unsigned int>> *tickets) {
    unsigned long n = 0;
    unsigned short i;

    // Iter over tickets.
    for (const vector<unsigned int>& t : *tickets) {
        i = ticket_is_correct(ranges, &t);
        n += 1-i;
    }

    return n;
}

unsigned long count_correct(const vector<vector<unsigned int>> *ranges, const vector<vector<unsigned int>> *tickets) {
    unsigned long n = 0;
    unsigned short i;

    // Iter over tickets.
    for (const vector<unsigned int>& t : *tickets) {
        i = ticket_is_correct(ranges, &t);
        n += i;
    }
    return n;
}

unsigned long count_correct2(const vector<vector<unsigned int>> *ranges, const vector<vector<unsigned int>> *tickets) {
    unsigned long n = 0;
    unsigned short i;

    // Iter over tickets.
    for (const vector<unsigned int>& t : *tickets) {
        n += ticket_is_correct2(ranges, &t);
    }
    return n;
}

unsigned short ticket_is_correct2(const vector<vector<unsigned int>> *ranges, const vector<unsigned int> *ticket) {
    unsigned int c = 0;
    unsigned short r1, r2;

    // check each field.
    for (unsigned int i = 0; i < ticket->size(); ++i) {
        vector<unsigned int> f = ranges->at(i);
        unsigned int num = ticket->at(i);

        r1 = (f.at(0) <= num && num <= f.at(1)) ? 1 : 0;
        r2 = (f.at(2) <= num && num <= f.at(3)) ? 1 : 0;
        c += r1 | r2;
    }

    // if (c < ticket->size()) cout << "Invalid: " << c << endl;
    return c == ticket->size() ? 1 : 0;
}
*/

int main() {
    unsigned long n;
    vector<vector<unsigned int>> ranges;
    vector<string> fields;
    vector<unsigned int> my_ticket;
    vector<vector<unsigned int>> tickets;

    // Part one.
    get_input(INPUT_PATH, &ranges, &fields, &tickets, &my_ticket);
    n = solution_1::count_error_rate(&ranges, &tickets);
    printf("Part 1: %lu\n", n);

    // Part two.
    n = solution_2::solve_2(&ranges, &fields, &tickets, &my_ticket);
    printf("Part 2: %lu\n", n);

    return 0;
}
