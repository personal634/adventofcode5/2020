//
// Created by imanol on 2/12/20.
//

// #define VERBOSE
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

const char* INPUT_PATH = "../input/input14";
const unsigned long ONES = (1ul << 36) - 1ul;

unsigned long solve_1(const char* filename) {

    // Memory variables.
    unordered_map<unsigned long, unsigned int> add2subadd;
    vector<unsigned long> mem;

    unsigned long mask_ones;
    unsigned long mask_zeros;
    unsigned long address, value;
    unsigned int p_add, p_asign, p_add_end;
    unsigned long long n;
    unsigned int i;
    char line[50];
    string s;

    mask_ones = 0ul;
    mask_zeros = 0ul;
    n = 0ul;

    // File.
    FILE* f = fopen(filename, "r");

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    while (fgets(line, 50, f)) {
        s = string(line);

        if (s.find('[') < s.size()) {
            p_add = s.find('[') + 1;
            p_add_end = s.find(']');
            p_asign = s.find('=') + 2;

            address = stoul(s.substr(p_add, p_add_end - p_add));
            value = stoul(s.substr(p_asign));

            value |= mask_ones;
            value &= mask_zeros;

            if (add2subadd.count(address) == 0) {

                add2subadd.insert(pair<unsigned long, unsigned int> (address, add2subadd.size()));
                mem.push_back(value);
            }
            else {
                mem[add2subadd[address]] = value;
            }
        }
        else {
            p_asign = s.find('=') + 2;
            s = s.substr(p_asign);
            for (i = 0u; i < 36u; ++i) {
                if (s.at(i) == 'X') {
                    mask_ones &= ((1ul << 36) - 1ul) - (1ul << (35 - i));
                    // mask_zeros &= ((1ul << 36) - 1ul) - (1ul << (35 - i));
                    mask_zeros |= 1ul << (35 - i);
                }
                if (s.at(i) == '0') {
                    mask_ones &= ((1ul << 36) - 1ul) - (1ul << (35 - i));
                    // mask_zeros |= 1ul << (35 - i);
                    mask_zeros &= ((1ul << 36) - 1ul) - (1ul << (35 - i));
                }
                else if (s.at(i) == '1') {
                    mask_ones |= 1ul << (35 - i);
                    // mask_zeros &= ((1ul << 36) - 1ul) - (1ul << (35 - i));
                    mask_zeros |= 1ul << (35 - i);
                }
            }
        }
    }

    fclose(f);
    for (unsigned long v : mem) n += (unsigned long long) v;
    return n;
}

void get_addresses(const unsigned long base_add, const unsigned long mask_ones, const unsigned long mask_float, unordered_set<unsigned long> *s) {
    *s = unordered_set<unsigned long> ();
    unsigned int i;
    unsigned long address;

    // Set ones on mask=1.
    address = base_add | mask_ones;

    // Expand addresses.
    s->insert(mask_float);
    for (i = 0; i < 36; ++i) {
        if (((1ul << i) & mask_float) != 0) {
            unordered_set<unsigned long> temp;
            for (unsigned long um : *s) {
                temp.insert(um | (1ul << i));
                temp.insert(um & (ONES - (1ul << i)));
            }
            swap(temp, *s);
        }
    }

    // Apply expanded masks to the address.
    unordered_set<unsigned long> temp;
    for (unsigned long expanded_mask_float : *s) {
        temp.insert((address | expanded_mask_float) - (address & expanded_mask_float));
    }

    swap(temp, *s);
}

void get_masks(const string *mask, unsigned long *mask_ones, unsigned long *mask_floats) {
    if(mask->size() < 36) {
        cout << "E: mask length less than 36" << endl;
        exit(1);
    }

    unsigned int i;
    *mask_ones = 0ul;
    *mask_floats = 0ul;
    for (i = 0u; i < 36u; ++i) {
        if (mask->at(i) == 'X') {
            // ones -> 0 (or), float -> 1 (check later).
            *mask_floats |= 1ul << (35 - i);
        }
        else if (mask->at(i) == '0') {
            // ones -> 0 (or), float -> 0.
        }
        else if (mask->at(i) == '1') {
            // ones -> 1 (or), float -> 0.
            *mask_ones |= 1ul << (35 - i);
        }
        else {
            cout << "Unknown character." << endl;
            exit(1);
        }
    }
}

unsigned long long solve_2(const char* filename) {
    // Memory variables.
    unordered_map<unsigned long, unsigned long> mem;

    unsigned long mask_ones;
    unsigned long mask_float;
    unsigned long address;
    unsigned long value;
    unsigned int p_add, p_assign, p_add_end;
    unsigned long n;
    char line[500];
    string s;
    n = 0ull;

    // File.
    FILE* f = fopen(filename, "r");

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    while (fgets(line, 500, f)) {
        s = string(line);
        p_assign = s.find('=') + 2;

        if (s.find('[') < s.size()) {
            p_add = s.find('[') + 1;
            p_add_end = s.find(']');

            address = stoul(s.substr(p_add, p_add_end - p_add));
            value = stoull(s.substr(p_assign));

            // Get the addresses.
            unordered_set<unsigned long> add_set;
            get_addresses(address, mask_ones, mask_float, &add_set);

            // Set values on addresses.
            for (unsigned long add : add_set) {
                mem[add] = value;
            }
        }
        else {
            s = s.substr(p_assign);
            get_masks(&s, &mask_ones, &mask_float);
        }
    }

    fclose(f);
    for (pair<unsigned long, unsigned long> p: mem) {
        n += p.second;
    }
    return n;
}

int main() {
    unsigned long n;

    // Part one.
    n = solve_1(INPUT_PATH);
    printf("Part 1: %lu\n", n);

    // Part two.
    n = solve_2(INPUT_PATH);
    printf("Part 2: %lu\n", n);

    return 0;
}
