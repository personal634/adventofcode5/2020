//
// Created by imanol on 30/04/21.
//

#include <iostream>
#include <vector>

using namespace std;

const char* INPUT_PATH = "../input/input23";

struct node {
    unsigned long value;
    node *next;
    node *prev;
};

class LinkedList {
public:
    node *current, *head, *tail;
private:

    LinkedList(node *current, node* head, node*tail) : current(current), head(head), tail(tail) {

    }
public:
    LinkedList() {
        current = nullptr;
        head = nullptr;
        tail = nullptr;
    }

    void add(unsigned long v) {
        node *tmp = new node;
        tmp->value = v;

        if (current == nullptr) {
            tmp->prev = tmp;
            tmp->next = tmp;
            current = tmp;
            head = tmp;
            tail = tmp;
        }
        else {
            // End tmp initialization.
            tmp->prev = head;
            tmp->next = head->next;

            // Break and link new head.
            head->next->prev = tmp;
            head->next = tmp;

            // Redefine head, current and tail.
            head = tmp;
            tail = tmp->prev;
            current = tmp;
        }
    }

    void next() {
        current = current->next;
        head = current;
        tail = current->prev;
    }

    /*
    void prev() {
        current = current->prev;
        head = current;
        tail = current->prev;
    }
     */

    node* begin() const {
        return this->head;
    }

    node* end() const {
        return this->tail;
    }

    node* at(unsigned long n) const {
        node* c = this->current;
        for (unsigned long i = 0; i < n; ++i) {
            c = c->next;
        }
        return c;
    }

    LinkedList extract(unsigned long n) {
        if (n == 0) return {};
        node *head_1 = this->current;
        node *head_2 = at(n);
        node *tail_1 = head_2->prev;
        node *tail_2 = this->tail;

        head_1->prev = tail_1;
        tail_1->next = head_1;
        head_2->prev = tail_2;
        tail_2->next = head_2;

        this->current = head_2;
        this->head = head_2;
        this->tail = tail_2;
        LinkedList new_linked_list = LinkedList(head_1, head_1, tail_1);
        return new_linked_list;
    }

    void join(LinkedList &ll2) const {
        if (ll2.current == nullptr) return;

        ll2.head->prev = this->tail;
        ll2.tail->next = this->head;
        this->tail->next = ll2.head;
        this->head->prev = ll2.tail;
    }

};

void get_input(const char* filename, LinkedList &ll) {
    // File.
    FILE* f = fopen(filename, "r");

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    char line[100];

    fgets(line, 100, f);
    string s (line);
    for (const char &c: s) {
        if (c != '\t' && c != '\n' && c != '\r' && c != '\0') {
            ll.add(c - '0');
        }
    }
    ll.next();
    fclose(f);
}

void get_input_2(const char* filename, LinkedList &ll) {
    // File.
    FILE* f = fopen(filename, "r");

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    char line[100];
    unsigned long n_max = 0;
    unsigned long sz = 0;

    fgets(line, 100, f);
    string s (line);
    for (const char &c: s) {
        if (c != '\t' && c != '\n' && c != '\r' && c != '\0') {
            ll.add(c - '0');
            n_max = n_max > ll.at(0)->value ? n_max : ll.at(0)->value;
            ++sz;
        }
    }

    ++n_max;
    for (; sz < 1000000; ++sz) {
        ll.add(n_max);
        ++n_max;
    }

    ll.next();
    fclose(f);
}

void make_a_move(LinkedList &ll, const unsigned long max_val) {
    unsigned long current_value, destination_value;
    bool match = true;
    node *iter_node, *current_node;

    // Get current value.
    current_value = ll.at(0)->value;
    current_node = ll.at(0);

    // Move to the next value.
    ll.next();
    // Extract three cups.
    LinkedList popped = ll.extract(3);

    // Get destination value.
    destination_value = ((current_value - 1 + max_val - 1) % max_val) + 1;
    while (match) {
        iter_node = popped.end();
        match = false;
        do {
            iter_node = iter_node->next;
            if (destination_value == iter_node->value) {
                match = true;
                break;
            }
        } while (iter_node != popped.end());
        if (match) destination_value = ((destination_value - 1 + max_val - 1) % max_val) + 1;
    }

    // Place destination cup at the list tail.
    while (ll.at(0)->value != destination_value) ll.next();
    ll.next();

    // Append.
    ll.join(popped);

    // Set cursor at cup next to current cap.
    // while (ll.at(0)->value != current_value) ll.next();
    // ll.next();
    ll.current = current_node->next;
    ll.head = current_node->next;
    ll.tail = current_node;
}

void make_a_move(LinkedList &ll, const unsigned long max_val, vector<node*> &nodes) {
    unsigned long current_value, destination_value;
    bool match = true;
    node *iter_node, *current_node;

    // Get current value.
    current_value = ll.at(0)->value;
    current_node = ll.at(0);

    // Move to the next value.
    ll.next();
    // Extract three cups.
    LinkedList popped = ll.extract(3);

    // Get destination value.
    destination_value = ((current_value - 1 + max_val - 1) % max_val) + 1;
    while (match) {
        iter_node = popped.end();
        match = false;
        do {
            iter_node = iter_node->next;
            if (destination_value == iter_node->value) {
                match = true;
                break;
            }
        } while (iter_node != popped.end());
        if (match) destination_value = ((destination_value - 1 + max_val - 1) % max_val) + 1;
    }

    // Place destination cup at the list tail.
    ll.tail = nodes[destination_value];
    ll.head = ll.tail->next;
    ll.current = ll.head;
    // while (ll.at(0)->value != destination_value) ll.next();
    // ll.next();

    // Append.
    ll.join(popped);

    // Set cursor at cup next to current cap.
    // while (ll.at(0)->value != current_value) ll.next();
    // ll.next();
    ll.current = current_node->next;
    ll.head = current_node->next;
    ll.tail = current_node;
}

unsigned long solve_1(LinkedList &ll, unsigned long n_moves) {
    unsigned long n = 0, max_val = 0;
    unsigned long i;
    node* iter_node = ll.end();

    // Get maximum value.
    do {
        iter_node = iter_node->next;
        max_val = max_val > iter_node->value ? max_val : iter_node->value;
    } while (iter_node != ll.end());

    for (i = 0; i < n_moves; ++i) {
        make_a_move(ll, max_val);
    }

    while (ll.at(0)->value != 1) ll.next();

    iter_node = ll.end();
    do {
        iter_node = iter_node->next;
        if (iter_node->value == 1) {
            continue;
        }
        else {
            n = n * 10 + iter_node->value;
        }
    } while (iter_node != ll.end());
    return n;
}

unsigned long solve_2(LinkedList &ll, unsigned long n_moves) {
    unsigned long n, max_val;
    unsigned long i = 1;
    node *iter_node = ll.end();
    vector<node*> nodes;

    nodes.reserve(10000000+1);
    nodes.push_back(nullptr);
    do {
        iter_node = iter_node->next;
        nodes[iter_node->value] = iter_node;
    } while (iter_node != ll.end());

    // Get maximum value.
    max_val = 0;
    do {
        iter_node = iter_node->next;
        max_val = max_val > iter_node->value ? max_val : iter_node->value;
    } while (iter_node != ll.end());

    for (i = 0; i < n_moves; ++i) {
        // make_a_move(ll, max_val, nodes);
        make_a_move(ll, max_val, nodes);
    }

    while (ll.at(0)->value != 1) ll.next();
    n = ll.at(1)->value * ll.at(2)->value;
    return n;
}

int main() {
    unsigned long n;
    LinkedList ll;
    LinkedList ll2;

    // Part one.
    get_input(INPUT_PATH, ll);
    n = solve_1(ll, 100);
    printf("Part 1: %lu\n", n);

    // Part 2.
    get_input_2(INPUT_PATH, ll2);
    n = solve_2(ll2, 10000000);
    printf("Part 2: %lu\n", n);

    return 0;
}
