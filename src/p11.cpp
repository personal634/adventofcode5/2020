//
// Created by imanol on 2/12/20.
//

#include <iostream>
#include <vector>

using namespace std;

const char* INPUT_PATH = "../input/input11";

// 0-> floor, 1-> occupied, 2-> free.

unsigned int count_adj(const unsigned short st, const unsigned long k, const unsigned long nx, const unsigned long ny, const vector<unsigned short> *v) {
    unsigned long i = k % nx, j = k / nx;
    unsigned int count = 0;

    // Horizontal.
    if (i < nx - 1) {
        count += v->at(k + 1) == st ? 1 : 0;
    }
    if (i > 0) {
        count += v->at(k - 1) == st ? 1 : 0;
    }

    // Vertical.
    if (j < ny - 1) {
        count += v->at(k + nx) == st ? 1 : 0;
    }
    if (j > 0) {
        count += v->at(k - nx) == st ? 1 : 0;
    }

    // Diagonal.
    if (i < nx-1 && j < ny-1) count += v->at(k+1+nx) == st ? 1 : 0;  // SE
    if (i < nx-1 && j > 0)    count += v->at(k+1-nx) == st ? 1 : 0;  // NE
    if (i > 0 && j < ny-1)    count += v->at(k-1+nx) == st ? 1 : 0;  // SW
    if (i > 0 && j > 0)       count += v->at(k-1-nx) == st ? 1 : 0;  // NW

    return count;
}

unsigned long norm0(const long i0, const long j0, const long in, const long jn) {
    return abs(in - i0) + abs(jn - j0);
}

unsigned long norm0_k(const unsigned long k_0, const unsigned long k_n, const unsigned long nx) {
    return norm0((long) (k_0 % nx), (long) (k_0 / nx), (long) (k_n % nx), (long) (k_n / nx));
}

unsigned int count_fill_dir(const signed long k, const unsigned long nx, const unsigned long ny, const vector<unsigned short> *v) {
    signed long temp_0, temp_n;
    signed int count = 0;
    signed long incs[8] = {
            +1,                     // E
            -1,                     // W
            + (signed long) nx,     // N
            - (signed long) nx,     // S
            +1 + (signed long) nx,  // NE
            +1 - (signed long) nx,  // SE
            -1 + (signed long) nx,  // NW
            -1 - (signed long) nx,  // SW
            };

    // Right.

    for (signed long inc : incs) {
        temp_0 = k;
        temp_n = k+inc;

        while (norm0_k(temp_0, temp_n, nx) <= 2 && temp_n >= 0 && temp_n < nx * ny) {
            if (v->at(temp_n) == 1) {
                count += 1;
                temp_n = -1;
            }
            else if (v->at(temp_n) == 0) {
                temp_0 = temp_n;
                temp_n += inc;
            }
            else {
                temp_n = -1;
            }
        }
    }

    return count;
}

void get_input(const char* filename, unsigned long *nx, unsigned long *ny, vector<unsigned short> *v) {
    FILE* f = fopen(filename, "r");
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    int c = getc(f);
    *nx = 0;
    *ny = 0;

    while(c != EOF) {
        if (c == '.') {
            v->push_back(0);
            if (*ny == 0) *nx += 1;
        }
        else if (c == '#') {
            v->push_back(1);
            if (*ny == 0) *nx += 1;
        }
        else if (c == 'L') {
            v->push_back(2);
            if (*ny == 0) *nx += 1;
        }
        else if(c == '\n') {
            *ny += 1;
        }

        c = getc(f);
    }
}

void print_puzzle(const unsigned long nx, const unsigned long ny, const vector<unsigned short> *v) {
    for (unsigned long j = 0; j < ny; ++j) {
        for (unsigned long i = 0; i < nx; ++i) {
            switch (v->at(i + j * nx)) {
                case 0:
                    cout << '.';
                    break;
                case 1:
                    cout << '#';
                    break;
                default:
                    cout << 'L';
                    break;
            }
            // cout << v->at(i + j * nx);
        }
        cout << endl;
    }
}

unsigned long solve_1(const unsigned long nx, const unsigned long ny, vector<unsigned short> *v) {
    unsigned long changed = 1, k;
    vector<unsigned short> clone;

    for (k = 0; k < nx * ny; ++k) clone.push_back(v->at(k));

    while (changed > 0) {
        changed = 0;


        for (k = 0; k < nx * ny; ++k) clone[k] = v->at(k);
        // cout << count_adj(1, 3, nx, ny, v) << endl;

        for (k = 0; k < nx * ny; ++k) {

            if (v->at(k) == 1) {
                if (count_adj(1, k, nx, ny, v) >= 4) {
                    clone[k] = 2;
                    changed += 1;
                }
            }
            else if (v->at(k) == 2) {
                if (count_adj(1, k, nx, ny, v) == 0) {
                    clone[k] = 1;
                    changed += 1;
                }
            }
        }

        for (k = 0; k < nx * ny; ++k) v->operator[](k) = clone[k];
        // print_puzzle(nx, ny, v);
        // cout << endl;
    }

    changed = 0;
    for (k = 0; k < nx * ny; ++k) if (v->at(k) == 1) changed += 1;
    return changed;
}

unsigned long solve_2(const unsigned long nx, const unsigned long ny, vector<unsigned short> *v) {
    unsigned long changed = 1;
    unsigned int c;
    signed long k;
    vector<unsigned short> clone;

    for (k = 0; k < nx * ny; ++k) clone.push_back(v->at(k));

    while (changed > 0) {
        changed = 0;

        for (k = 0; k < nx * ny; ++k) clone[k] = v->at(k);
        // cout << count_adj(1, 3, nx, ny, v) << endl;

        for (k = 0; k < nx * ny; ++k) {
            c = count_fill_dir(k, nx, ny, v);
            if (v->at(k) == 1 && c >= 5) {
                clone[k] = 2;
                changed += 1;
            }
            else if (v->at(k) == 2 && c == 0) {
                clone[k] = 1;
                changed += 1;
            }

            // cout << c;
            // if (k / nx != (k+1) / nx) cout << endl;
        }

        for (k = 0; k < nx * ny; ++k) v->operator[](k) = clone[k];
        // print_puzzle(nx, ny, v);
        // cout << endl;
        // print_puzzle(nx, ny, v);
        // cout << endl;
    }

    changed = 0;
    for (k = 0; k < nx * ny; ++k) if (v->at(k) == 1) changed += 1;
    return changed;
}

int main() {
    unsigned long nx, ny, n;
    vector<unsigned short> grid;

    // Part one.
    get_input(INPUT_PATH, &nx, &ny, &grid);
    n = solve_1(nx, ny, &grid);
    printf("Part 1: %lu\n", n);
    // print_puzzle(nx, ny, &grid);

    // Part two.
    while (!grid.empty()) grid.pop_back();
    get_input(INPUT_PATH, &nx, &ny, &grid);
    n = solve_2(nx, ny, &grid);
    printf("Part 2: %lu\n", n);
    // print_puzzle(nx, ny, &grid);

    return 0;
}
