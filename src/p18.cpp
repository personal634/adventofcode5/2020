//
// Created by imanol on 2/12/20.
//

#include <iostream>

using namespace std;

const char* INPUT_PATH = "../input/input18";

enum Operator {
    ADD,
    MULT,
};

signed long parse_operation(string *s) {
    signed long n;
    signed long t1, t2, temp;
    unsigned long p_operator;
    unsigned long p_par_start = 0, p_counter;
    unsigned long k;
    bool op_mark = false, t1_initialized = false, par_init = false;
    Operator op;

    while (s->find('(') < s->size()) {
        p_counter = 0;
        for (k = 0; k < s->size(); ++k) {
            if (s->at(k) == '(') {
                if (not par_init) {
                    p_par_start = k;
                    par_init = true;
                }
                p_counter += 1;
            }
            else if (s->at(k) == ')') {
                p_counter -= 1;
            }

            if (p_counter == 0 && par_init) {
                string s2 = s->substr(p_par_start + 1, k - 1 - p_par_start);
                temp = parse_operation(&s2);
                s2 = to_string(temp);
                s->replace(p_par_start, k + 1 - p_par_start, s2);
                par_init = false;
                k = 0;
            }
        }
    }

    if (s->find('+') > s->size() && s->find('*') > s->size()) {
        return stol(*s);
    }

    for (k = 0; k < s->size(); ++k) {
        if (s->at(k) == '+' || s->at(k) == '*') {
            if (not t1_initialized) {
                t1 = stol(s->substr(0, k-1));
                t1_initialized = true;
            }
            else {
                t2 = stol(s->substr(p_operator+1, k-1-p_operator));
            }

            p_operator = k;

            if (op_mark) {
                if (op == Operator::ADD) {
                    t1 = t1 + t2;
                }
                else {
                    t1 = t1 * t2;
                }
                if (s->at(k) == '+') {
                    op = Operator::ADD;
                }
                else {
                    op = Operator::MULT;
                }
            }
            else {
                if (s->at(k) == '+') {
                    op = Operator::ADD;
                }
                else {
                    op = Operator::MULT;
                }
                op_mark = true;
            }
        }
    }

    if (op == Operator::ADD) {
        n = t1 + stol(s->substr(p_operator+1));
    }
    else {
        n = t1 * stol(s->substr(p_operator+1));
    }

    return n;
}

signed long parse_operation_2(string *s) {
    signed long t1, t2, temp;
    unsigned long p_operator, p_p, p_n;
    unsigned long p_par_start = 0, p_counter;
    unsigned long k;
    bool par_init = false;

    // Parse parenthesis.
    while (s->find('(') < s->size()) {
        p_counter = 0;
        for (k = 0; k < s->size(); ++k) {
            if (s->at(k) == '(') {
                if (not par_init) {
                    p_par_start = k;
                    par_init = true;
                }
                p_counter += 1;
            }
            else if (s->at(k) == ')') {
                p_counter -= 1;
            }

            if (p_counter == 0 && par_init) {
                string s2 = s->substr(p_par_start + 1, k - 1 - p_par_start);
                temp = parse_operation_2(&s2);
                s2 = to_string(temp);
                s->replace(p_par_start, k + 1 - p_par_start, s2);
                par_init = false;
                k = 0;
            }
        }
    }

    // Parse sum.
    while (s->find('+') < s->size()) {
        p_operator = s->find('+');
        p_n = min(s->find('+', p_operator+1), s->find('*', p_operator+1));
        unsigned long p_p_p = s->rfind('+', p_operator-1);
        unsigned long p_p_m = s->rfind('*', p_operator-1);
        if (p_p_p > s->size() && p_p_m > s->size()) {
            p_p = p_p_p;
        }
        else if (p_p_p > s->size()) {
            p_p = p_p_m;
        }
        else if (p_p_m > s->size()) {
            p_p = p_p_p;
        }
        else {
            p_p = max(p_p_p, p_p_m);
        }

        if (p_p >= s->size()) {
            t1 = stol(s->substr(0, p_operator - 1));
        }
        else {
            t1 = stol(s->substr(p_p+1, p_operator-1-p_p));
        }

        if (p_n >= s->size()) {
            t2 = stol(s->substr(p_operator + 1));
        }
        else {
            t2 = stol(s->substr(p_operator+1, p_n - 1 - p_operator));
        }
        string s2 = to_string(t1 + t2);

        if (p_p >= s->size() && p_n >= s->size()) {
            s->replace(0, s->size(), s2);
        }
        else if (p_p >= s->size()) {
            s->replace(0, p_n - 1, s2);
        }
        else if (p_n >= s->size()) {
            s->replace(p_p + 1, s->size() - p_p, s2);
        }
        else {
            s->replace(p_p + 1, p_n - 1 - p_p, s2);
        }
    }

    // Parse multiplication.
    while (s->find('*') < s->size()) {
        p_operator = s->find('*');
        p_n = min(s->find('+', p_operator+1), s->find('*', p_operator+1));
        unsigned long p_p_p = s->rfind('+', p_operator-1);
        unsigned long p_p_m = s->rfind('*', p_operator-1);
        if (p_p_p > s->size() && p_p_m > s->size()) {
            p_p = p_p_p;
        }
        else if (p_p_p > s->size()) {
            p_p = p_p_m;
        }
        else if (p_p_m > s->size()) {
            p_p = p_p_p;
        }
        else {
            p_p = max(p_p_p, p_p_m);
        }

        if (p_p >= s->size()) {
            t1 = stol(s->substr(0, p_operator - 1));
        }
        else {
            t1 = stol(s->substr(p_p+1, p_operator-1-p_p));
        }

        if (p_n >= s->size()) {
            t2 = stol(s->substr(p_operator + 1));
        }
        else {
            t2 = stol(s->substr(p_operator+1, p_n - 1 - p_operator));
        }
        string s2 = to_string(t1 * t2);

        if (p_p >= s->size() && p_n >= s->size()) {
            s->replace(0, s->size(), s2);
        }
        else if (p_p >= s->size()) {
            s->replace(0, p_n - 1, s2);
        }
        else if (p_n >= s->size()) {
            s->replace(p_p + 1, s->size() - p_p, s2);
        }
        else {
            s->replace(p_p + 1, p_n - 1 - p_p, s2);
        }
    }

    // No more operations, return result.
    return stol(*s);
}

signed long solve_1(const char* filename) {
    // File.
    FILE* f = fopen(filename, "r");
    signed long n = 0;

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    char line[800];
    string s;

    while (fgets(line, 800, f)) {
        s = string (line);
        n += parse_operation(&s);
        // cout << n << endl;
    }

    fclose(f);
    return n;
}

signed long solve_2(const char* filename) {
    // File.
    FILE* f = fopen(filename, "r");
    signed long n = 0;

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    char line[800];
    string s;

    while (fgets(line, 800, f)) {
        s = string (line);
        n += parse_operation_2(&s);
    }

    fclose(f);
    return n;
}

int main() {
    unsigned long n;

    // Part one.
    n = solve_1(INPUT_PATH);
    printf("Part 1: %lu\n", n);

    // Part two.
    n = solve_2(INPUT_PATH);
    printf("Part 2: %lu\n", n);

    return 0;
}
