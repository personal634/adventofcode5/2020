//
// Created by imanol on 2/12/20.
//

#include <iostream>
// #include <cstring>

using namespace std;

const char* INPUT_PATH = "../input/input06";

unsigned long get_quiz_answers(const char* filename) {
    FILE* f = fopen(filename, "r");
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    unsigned long n = 0, p, answers = 0;
    char line[500];

    while( fgets(line, 100, f) ) {
        if (line[0] == '\n') {
            while (answers) {
                n += answers & 1u;
                answers = answers >> 1;
            }
            // cout << n << endl;
        }
        else {
            p = 0;
            while('a' <= line[p] && 'z' >= line[p]) {
                answers = answers | (1 << (line[p] - 'a'));
                p += 1;
            }
        }
    }
    while (answers) {
        n += answers & 1u;
        answers = answers >> 1;
    }
    // cout << n << endl;
    // cout << answers << " remains" << endl;

    fclose(f);
    return n;
}

unsigned long get_common_quiz_answers(const char* filename) {
    FILE* f = fopen(filename, "r");
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    unsigned long n = 0, p, answers = 0;
    unsigned int char_counter[27];
    unsigned int ptps = 0;
    char line[500];
    unsigned int i;

    for(i = 0; i < 27; ++i) char_counter[i] = 0;

    while( fgets(line, 100, f) ) {
        if (line[0] == '\n' || line[0] == EOF) {
            for(i = 0; i < 27; ++i) {
                if (char_counter[i] == ptps) n += 1;
                char_counter[i] = 0;
            }
            ptps = 0;
        }
        else {
            p = 0;
            ptps += 1;
            while('a' <= line[p] && 'z' >= line[p]) {
                char_counter[line[p] - 'a'] += 1;
                p += 1;
            }
        }
    }

    if(ptps != 0) {
        // cout << "Non zero" << endl;
        for(i = 0; i < 27; ++i) {
            if (char_counter[i] == ptps) n += 1;
            char_counter[i] = 0;
        }
        ptps = 0;
    }
    fclose(f);
    return n;
}

int main() {
    unsigned long n;

    // Part one.
    n = get_quiz_answers(INPUT_PATH);
    printf("Part 1: %lu\n", n);

    // Part two.
    n = get_common_quiz_answers(INPUT_PATH);
    printf("Part 2: %lu\n", n);

    return 0;
}