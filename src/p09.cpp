//
// Created by imanol on 2/12/20.
//

#include <iostream>
#include <unordered_set>
// #include <cstring>

using namespace std;

const char* INPUT_PATH = "../input/input09";
const unsigned int PREAMBLE = 25;

void reset_to_line(FILE* f, const unsigned int line_no) {
    rewind(f);
    char c[500];
    for (int i = 1; i < line_no; ++i) fgets(c, 500, f);
}

unsigned int get_line_number(FILE* f) {
    unsigned int n = 0;
    char c[500];

    rewind(f);

    while(fgets(c, 500, f)) n += 1;
    return n;
}

unsigned long get_xmas_err(const char* filename, const int preamble) {
    FILE* f = fopen(filename, "r");
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    unsigned long xmas_values[preamble];
    unsigned long xmas_value = 0;
    char line[500];
    unsigned int p = 0, first_preamble = 0;
    unsigned int i, j, success_flag;

    while ( fgets(line, 500, f) ) {
        xmas_value = stoul(line);

        if (first_preamble == 0) {
            xmas_values[p] = xmas_value;
            p += 1;
            if (p == preamble) first_preamble += 1;
            p = p % preamble;
        }
        else {
            i = 0;
            j = 0;
            success_flag = 0;

            while (success_flag == 0) {
                j = j + 1;
                if (j == preamble) {
                    i += 1;
                    j = i + 1;
                }
                if (i == preamble - 1) return xmas_value;

                if (xmas_values[i] == xmas_values[j]) {
                    continue;
                }
                else {
                    if (xmas_values[i] + xmas_values[j] == xmas_value) {
                        success_flag = 1;
                    }
                    else {
                        continue;
                    }
                }
            }

            xmas_values[p] = xmas_value;
            p += 1;
            p = p % preamble;
        }
    }
    return xmas_value;
}

unsigned long get_xmas_set(const char* filename, const unsigned long sum) {
    FILE* f = fopen(filename, "r");
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    unsigned long n_lines = get_line_number(f);
    unsigned long c_sum, c_line = 1, c_min, c_max, c_val, s_n;
    char line[500];

    rewind(f);

    while (c_line < n_lines) {
        c_sum = 0;
        c_min = -1;
        c_max = 0;
        s_n = 0;

        reset_to_line(f, c_line);

        while (c_sum < sum) {
            fgets(line, 500, f);
            c_val = stoul(line);
            c_sum += c_val;
            c_min = c_val < c_min ? c_val : c_min;
            c_max = c_val > c_max ? c_val : c_max;
            s_n += 1;
        }

        if (c_sum == sum && s_n >= 2) {
            // cout << c_min << " " << c_max << endl;
            return c_min + c_max;
        }

        c_line += 1;
    }

    return 0;
}

int main() {
    unsigned long n = 0;
    unsigned int jmp = 0;

    // Part one.
    n = get_xmas_err(INPUT_PATH, PREAMBLE);
    printf("Part 1: %lu\n", n);

    // Part two.
    n = get_xmas_set(INPUT_PATH, n);
    printf("Part 2: %lu\n", n);

    return 0;
}