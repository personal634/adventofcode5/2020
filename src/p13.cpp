//
// Created by imanol on 2/12/20.
//

#include <iostream>
#include <vector>

using namespace std;

const char* INPUT_PATH = "../input/input13";

void get_input(const char* filename, unsigned long long *timestamp, vector<vector<unsigned int>> *ids) {
    char line[500];
    unsigned int p0 = 0, pc, offset = 0;

    // File.
    FILE* f = fopen(filename, "r");

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    // Get timestamp.
    fgets(line, 500, f);
    *timestamp = stoi(line);

    // Get IDs.
    fgets(line, 500, f);
    string s (line);
    while (pc = s.find(',', p0+1), pc < s.size()) {
        if (s.substr(p0, pc-p0) != "x") {
            vector<unsigned int> value;
            value.push_back(stoi(s.substr(p0, pc - p0)));
            value.push_back(offset);
            // value[0] = stoi(s.substr(p0, pc - p0));
            // value[1] = offset;
            ids->push_back(vector<unsigned int> (value));
        }
        p0 = pc + 1;
        offset += 1;
    }

    if (s.substr(p0) != "x") {
        vector<unsigned int> value;
        value.push_back(stoi(s.substr(p0)));
        value.push_back(offset);
        // value[0] = stoi(s.substr(p0));
        // value[1] = offset;
        ids->push_back(vector<unsigned int> (value));
    }
}

unsigned long long gcd(const unsigned long long a1, const unsigned long long a2) {
    unsigned long long c_min;
    unsigned long long c_max;
    unsigned long long res;

    if (a1 > a2) {
        c_min = a1;
        res = a2;
    }
    else {
        c_min = a2;
        res = a1;
    }

    do {
        c_max = c_min;
        c_min = res;
        res = c_max % c_min;

    } while (res != 0);

    return c_min;
}

unsigned long long lcm(const unsigned long long a1, const unsigned long long a2) {
    // cout << a1 << " " << a2 << " " << (a1 * a2) / gcd(a1, a2) << endl;
    return (a1 * a2) / gcd(a1, a2);
}

unsigned long solve_1(const unsigned long long timestamp, const vector<vector<unsigned int>> *ids) {
    // Solution: best bus ID and wait time.
    unsigned int min_id = 0;
    unsigned long long min_wait = -1;
    // Temp variables.
    unsigned long long c_wait;

    for (vector<unsigned int> id: *ids) {
        c_wait = (((timestamp / id.at(0)) + 1) * id.at(0) - timestamp) % id.at(0);
        if (c_wait < min_wait) {
            min_id = id[0];
            min_wait = c_wait;
        }
    }

    return (unsigned long long) min_id * min_wait;
}

void bubble_sort_rev(vector<vector<unsigned int>> * v) {
    unsigned long n = v->size();
    unsigned short swapped;
    unsigned long i;
    vector<unsigned int> temp;

    do {
        swapped = 0;
        for(i = 1; i < n; ++i) {
            if (v->at(i-1).at(0) < v->at(i).at(0)) {
                temp = v->at(i-1);
                v->operator[](i-1) = v->at(i);
                v->operator[](i) = temp;
                swapped = 1;
            }
        }
    } while (swapped == 1);
}

unsigned long long solve_2(const vector<vector<unsigned int>> *ids) {
    unsigned long long t = 0, dt = 1;

    for (vector<unsigned int> id : *ids) {
        while (t % id.at(0) != (id.at(0) - id.at(1)) % id.at(0)) t += dt;
        dt = lcm(dt, id.at(0));
    }

    return t;
}

int main() {
    unsigned long long timestamp, n;
    vector<vector<unsigned int>> ids;

    // Part one.
    get_input(INPUT_PATH, &timestamp, &ids);
    n = solve_1(timestamp, &ids);
    printf("Part 1: %llu\n", n);

    // Part two.
    bubble_sort_rev(&ids);
    for (int i = 0; i < ids.size(); ++i) {
        ids[i][1] = ids[i][1] % ids[i][0];
    }
    n = solve_2(&ids);
    printf("Part 2: %llu\n", n);

    return 0;
}
