//
// Created by imanol on 2/12/20.
//

#include <iostream>
#include <vector>
#include <unordered_map>
#include <unordered_set>

using namespace std;

const char* INPUT_PATH = "../input/input19";


void get_rule_2(const unsigned int rule_no, const unordered_map<unsigned int, string> *inp, unordered_map<unsigned int, vector<vector<string>>> *map) {
    string law = string (inp->at(rule_no));
    if (rule_no == 8) law = string (" 42 | 42 8");
    if (rule_no == 11) law = string ("42 31 | 42 11 31");
    string new_rule_no;
    vector<vector<string>> v;
    unsigned int p_space, p_num, p_pipe = 0;

    // This rule was set, skip.
    if (map->count(rule_no) > 0) return;

    // Law is either "a" or "b", set this value and exit.
    if (law.find('"') < law.size()) {
        unsigned int p_q_1 = law.find('"');
        unsigned int p_q_2 = law.find('"', p_q_1 + 1);
        vector<string> v2;
        v2.push_back(law.substr(p_q_1 + 1, p_q_2 - 1 - p_q_1));
        v.push_back(v2);
        map->insert(pair<unsigned int, vector<vector<string>>> (rule_no, v));
        return;
    }

    // Trim leading and trailing spaces and put an space at the end.
    while (law.at(0) == ' ') law = law.substr(1);
    while (law.at(law.size()-1) == ' ' || law.at(law.size()-1) == '\n') law = law.substr(0, law.size()-1);
    law.push_back(' ');

    // Split the rules by the '|' characters.
    vector<string> sub_laws;
    do {
        sub_laws.push_back(law.substr(p_pipe, law.find('|', p_pipe + 1)));
        p_pipe = law.find('|', p_pipe + 1) + 1;
    } while(p_pipe < law.size() && p_pipe > 0);

    // Split the rules into rule numbers.
    vector<vector<string>> sub_rules;
    for (string sub_law : sub_laws) {
        vector <string> vv;
        while (sub_law.at(0) == ' ') sub_law= sub_law.substr(1);
        p_num = 0;
        p_space = sub_law.find(' ', p_num + 1);
        do {
            vv.push_back(sub_law.substr(p_num, p_space - p_num));
            p_num = p_space + 1;
            p_space = sub_law.find(' ', p_num + 1);

        } while (p_space < sub_law.size());
        sub_rules.push_back(vv);
    }

    // Store result into map.
    map->insert(pair<unsigned int, vector<vector<string>>> (rule_no, sub_rules));
}


void get_rules_2(const char* filename, unordered_map<unsigned int, vector<vector<string>>> *rules) {
    // File.
    FILE* f = fopen(filename, "r");
    unordered_map<unsigned int, string> rules_raw;
    // unordered_map<unsigned int, vector<string>> rules;
    unsigned long p_colon;
    unsigned int rule_no;
    string raw;

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }
    char line[800];
    string s;

    // Get rules (raw).
    while (fgets(line, 800, f)) {
        s = string (line);
        if (p_colon = s.find(':'), p_colon > s.size()) break;
        rule_no = stoi(s.substr(0, p_colon));
        if (rule_no == 8) {
            raw = string(" 42 | 42 8");
        }
        else if (rule_no == 11) {
            raw = string(" 42 31 | 42 11 31");
        }
        else {
            raw = s.substr(p_colon+1);
        }
        rules_raw.insert(pair<unsigned int, string>(rule_no, raw));
    }
    fclose(f);

    for (pair<unsigned int, string> rn : rules_raw) {
        get_rule_2(rn.first, &rules_raw, rules);
    }
}


// PART 1


//! For each rule, expand its pattern for getting all the combinations and store them in a map.
void get_rule(const unsigned int rule_no, const unordered_map<unsigned int, string> *inp, unordered_map<unsigned int, vector<string>> *map) {
    string law = string (inp->at(rule_no));
    string new_rule_no;
    vector<string> v;
    unsigned int p_space, p_num, p_pipe = 0;

    // This rule was set, skip.
    if (map->count(rule_no) > 0) return;

    // Law is either "a" or "b", set this value and exit.
    if (law.find('"') < law.size()) {
        unsigned int p_q_1 = law.find('"');
        unsigned int p_q_2 = law.find('"', p_q_1 + 1);
        v.push_back(law.substr(p_q_1 + 1, p_q_2 - 1 - p_q_1));
        map->insert(pair<unsigned int, vector<string>> (rule_no, v));
        return;
    }

    // Trim leading and trailing spaces and put an space at the end.
    while (law.at(0) == ' ') law = law.substr(1);
    while (law.at(law.size()-1) == ' ' || law.at(law.size()-1) == '\n') law = law.substr(0, law.size()-1);
    law.push_back(' ');

    // Split the rules by the '|' characters.
    vector<string> sub_laws;
    do {
        sub_laws.push_back(law.substr(p_pipe, law.find('|', p_pipe + 1)));
        p_pipe = law.find('|', p_pipe + 1) + 1;
    } while(p_pipe < law.size() && p_pipe > 0);

    // Split the rules into rule numbers.
    vector<vector<unsigned int>> sub_rules;
    for (string sub_law : sub_laws) {
        vector <unsigned int> vv;
        while (sub_law.at(0) == ' ') sub_law= sub_law.substr(1);
        p_num = 0;
        p_space = sub_law.find(' ', p_num + 1);
        do {
            vv.push_back((unsigned int) stoi(sub_law.substr(p_num, p_space - p_num)));
            p_num = p_space + 1;
            p_space = sub_law.find(' ', p_num + 1);

        } while (p_space < sub_law.size());
        sub_rules.push_back(vv);
    }

    // Combine rules.
    // Desired patterns.
    vector<string> r;
    // Iter over current rule number's sub-rule numbers.
    for (const vector<unsigned int>& sub_rule : sub_rules) {
        // Iter over rule numbers.
        vector<string> it;
        it.emplace_back("");
        for (unsigned int sub_rule_no : sub_rule) {
            get_rule(sub_rule_no, inp, map);
            vector<string> patterns = map->operator[](sub_rule_no);

            vector<string> temp;

            for (const string& prefix : it) {
                for (const string& suffix : patterns) {
                    temp.push_back(prefix + suffix);
                }
            }

            swap(temp, it);
        }

        for (const string& results : it) {
            r.push_back(results);
        }
    }

    // Store result into map.
    map->insert(pair<unsigned int, vector<string>> (rule_no, r));
}


//! Get message rules from a filename and store it in a map where each rule (the key)
//! contains its matching pattern.
void get_raw_rules(const char* filename, unordered_map<unsigned int, string> *raw_rules) {
    // File.
    FILE* f = fopen(filename, "r");
    unsigned long p_colon;
    unsigned int rule_no;
    string raw;

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }
    char line[800];
    string s;

    // Get rules (raw).
    while (fgets(line, 800, f)) {
        s = string (line);
        if (p_colon = s.find(':'), p_colon > s.size()) break;
        rule_no = stoi(s.substr(0, p_colon));
        raw = s.substr(p_colon+1);
        raw_rules->insert(pair<unsigned int, string>(rule_no, raw));
    }
    fclose(f);
}

//! Get message rules from a filename and store it in a map where each rule (the key)
//! contains all the available patterns for fulfilling said rule.
void get_rules(const char* filename, unordered_map<unsigned int, vector<string>> *rules) {
    unordered_map<unsigned int, string> rules_raw;
    get_raw_rules(filename, &rules_raw);
    get_rule(0, &rules_raw, rules);
}

//! Get the input messages.
void get_data(const char* filename, vector<string> *data) {
    // File.
    FILE* f = fopen(filename, "r");

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }
    char line[800];
    string s;

    // Get rules (raw).
    while (fgets(line, 800, f)) {
        if (line[0] != 'a' && line[0] != 'b') continue;
        s = string (line);
        while (s.at(s.size() - 1) == ' ' || s.at(s.size() - 1) == '\n') s.erase(s.size() - 1);
        data->push_back(s);
    }
    fclose(f);
}

//! Return 1 if value matches any pattern in patts, else 0.
unsigned short match_patterns(const string &value, const vector<string> &patts) {
    for (const string& patt : patts) {
        if (value == patt) return 1;
    }
    return 0;
}

//! Count the number of messages in data that matches any pattern in rules.
unsigned long solve_1(const vector<string> &rules, const vector<string> &data) {
    unsigned long n = 0;
    for (const string& v : data) {
        n += match_patterns(v, rules);
    }
    return n;
}

// PART 2

/*
unsigned short match_recursively(const unsigned int rule_no, const string *value, unsigned int *pos, const unordered_map<unsigned int, vector<vector<string>>> *rules) {
    // Check that position pointer does not overflow.
    if (*pos >= value->size()) return 0;
    // Check length.
    if (value->size() == 0) return 1;

    // Check laws.
    unsigned int pos_temp = *pos, last_pos;
    unsigned short n;
    vector<vector<string>> laws = rules->at(rule_no);

    // Iter over sub-rules.
    for (const vector<string>& law : laws) {
        // Restore initial position.
        *pos = pos_temp;
        bool any_false = false;

        // Iter over each sub-rule's rules.
        for (string rule_no_str : law) {

            // Check if pos is out of bounds.
            if (*pos >= value->size()) {
                n = 1;
            }
            //  Check if rule is character a.
            else if (rule_no_str.at(0) == 'a'){
                n = value->at(*pos) == 'a' ? 1 : 0;
            }
            // Check if rule is character b.
            else if (rule_no_str.at(0) == 'b') {
                n = value->at(*pos) == 'b' ? 1 : 0;
            }
            // Check sub-rule's rule.
            else {
                n = match_recursively(stoi(rule_no_str), value, pos, rules);
            }
            // A rule was not met, invalid rule.
            if (n == 0) any_false = true;
            // Increase position.
            *pos += 1;
        }
        // The whole sub-rule was successful, pass 1.
        if (not any_false) return 1;
    }
    // Invalid, restore pos.
    *pos = pos_temp;
    return 0;
}

unsigned short match_v2(unsigned int *pos, vector<unsigned int> *counter, const string *value, const unsigned int rule_no, const unordered_map<unsigned int, vector<vector<string>>> *rules) {
    unsigned int m = 0;
    const vector<vector<string>> &sub_rules = rules->at(rule_no);
    const unsigned int temp = *pos;
    vector<unsigned int> temp_count;
    for (unsigned int v : *counter) temp_count.push_back(v);
    // Save any matching sub-rules.
    vector<unsigned int> good_counts;
    vector<unsigned int> good_pos;
    vector<bool> good_sub_rules;

    // Iter over options.
    for (const vector<string> &option : sub_rules) {
        // This option is too long, skip.
        if (temp + option.size() > value->size()) continue;
        // Add sub-rule's length to the count, and check that size does not overflow.
        *counter = temp_count + option.size();
        if (*counter > value->size()) continue;
        // Set this sub-rule state as valid (yet).
        unsigned int n = 1;
        // Reset original position and count.
        *pos = temp;

        // Iter over rules.
        for (const string &r_no : option) {
            // Match character.
            if (r_no.at(0) == 'a') {
                n &= value->at(*pos) == 'a' ? 1 : 0;
                *pos += 1;
            }
            // Match character.
            else if (r_no.at(0) == 'b') {
                n &= value->at(*pos) == 'b' ? 1 : 0;
                *pos += 1;
            }
            // Match other rule.
            else {
                unsigned int sub_rule_no = stoi(r_no);
                *counter -= 1;
                n &= match_v2(pos, counter, value, sub_rule_no, rules);
            }
        }

        m += n;

        if (n == 1) {
            good_sub_rules.push_back(true);
            good_counts.push_back(*counter);
            good_pos.push_back(*pos);
        }
        else {
            good_sub_rules.push_back(false);
        }
    }

    if (!good_pos.empty()) {
        for (unsigned int g : good_counts) {
            cout << g << ' ';
        }
        cout << '-' << value->size() << '-' << endl;
    }

    if (good_pos.empty()) {
        *pos = temp;
        return 0;
    }
    else if (good_pos.size() == 1) {
        *pos = good_pos.at(0);
        *counter = good_counts.at(0);
        return 1;
    }
    else {
        *pos = good_pos.at(0);
        *counter = good_counts.at(0);
        return 1;
    }
}

unsigned long solve_2(const unordered_map<unsigned int, vector<vector<string>>> *rules, vector<string> *data) {
    unsigned long n = 0;
    unsigned int p0, c;
    unsigned int i;
    for (const string& v : *data) {
        p0 = 0;
        c = 0;
        // n += match_recursively(0, &v, &p0, rules);
        i = match_v2(&p0, &c, &v, 0, rules);
        n += i;
        if (i == 1) cout << v << endl;
    }
    return n;
}

//! Split original pattern into its '|'-separated sub-patterns.
void pattern_or_split(const string &pattern, vector<string> &sub_patterns) {
    unsigned long p_start = 0;
    unsigned long p_end = 0;

    while (p_end = pattern.find('|', p_start), p_end <= pattern.length()) {
        sub_patterns.push_back(pattern.substr(p_start, p_end - p_start + 1));
        p_start = p_end + 1;
    }
}

//! If the message matches the pattern according to the raw_rules, return 1, else 0. An index
//! parameter with initial value of 0 is required for iteration purposes.
unsigned int match_rec(const string &pattern, const string &message, const unordered_map<unsigned int, string> &raw_rules, unsigned long &idx) {
    vector<string> sub_patterns;
    pattern_or_split(pattern, sub_patterns);

    // Iter over available sub-patterns.
    for (const string &sub_pattern: sub_patterns) {
        vector<string> sub_rules;
        simple_pattern_split(sub_pattern, sub_rules);

        for (const string &sub_rule: sub_rules) {
            if (sub_rule.at(0) == 'a') {
                if (message.at(idx) == 'a') {
                    idx += 1;
                }
                else {
                    break;
                }
            }
            else if (sub_rule.at(0) == 'b') {
                if (message.at(idx) == 'b') {
                    idx += 1;
                }
                else {
                    break;
                }
            }
            else {

            }
        }

    }

    // ...

    return 0;
}

//! Trim string.
void trim(string &s) {
    while (s.at(0) == ' ') {
        s = s.substr(1);
    }
    while (s.at(s.length() - 1) == ' ') {
        s = s.substr(0, s.length() - 1);
    }
}

//! Delete trailing and leading ".
void uncomment(string &s) {
    trim(s);
    while (s.at(0) == '"') {
        s = s.substr(1);
    }
    while (s.at(s.length() - 1) == '"') {
        s = s.substr(0, s.length() - 1);
    }
}

//! Split original pattern into its '|'-separated sub-patterns.
void pattern_or_split(const string &pattern, vector<string> &sub_patterns) {
    unsigned long p_start = 0;
    unsigned long p_end;
    string s;

    while (p_end = pattern.find('|', p_start), p_end <= pattern.length()) {
        s = pattern.substr(p_start, p_end - p_start);
        trim(s);
        sub_patterns.push_back(s);
        p_start = p_end + 1;
    }

    s = pattern.substr(p_start, pattern.length() - p_start + 1);
    trim(s);
    sub_patterns.push_back(s);
}

//! Split simple pattern (patterns without '|') into its sub-patterns (without spaces).
void simple_pattern_split(const string &pattern, vector<string> &sub_patterns) {
    unsigned long p_start = 0;
    unsigned long p_end;
    string s;

    while (p_end = pattern.find(' ', p_start + 1), p_end <= pattern.length()) {
        if (p_start + 1 != p_end) {
            s = pattern.substr(p_start, p_end - p_start + 1);
            uncomment(s);
            sub_patterns.push_back(s);
        }
        p_start = p_end;
    }

    s = pattern.substr(p_start, pattern.length() - p_start + 1);
    uncomment(s);
    sub_patterns.push_back(s);
}

//! Mark recursive rules.
void is_rule_recursive(const unsigned int rule_no, const unordered_map<unsigned int, string> &raw_rules, unordered_map<unsigned int, bool> &rec, unordered_set<unsigned int> &marks) {
    if (marks.count(rule_no) != 0) {
        rec.insert(pair<unsigned int, bool> (rule_no, true));
        return;
    }

    marks.insert(rule_no);

    vector<string> sub_patterns;
    // cout << "Original pattern: '" << raw_rules.at(rule_no) << "'" << endl;
    pattern_or_split(raw_rules.at(rule_no), sub_patterns);

    unsigned int count_rec = 0;
    for (const string &sub_pattern: sub_patterns) {
        // cout << "Sub-pattern: '" << sub_pattern << "'" << endl;
        vector<string> sub_rules;
        simple_pattern_split(sub_pattern, sub_rules);

        for (const string &new_rule: sub_rules) {
            // cout << "New rule: '" << new_rule << "'" << endl;
            if (new_rule.at(0) != 'a' && new_rule.at(0) != 'b') {
                unsigned int new_rule_int = stoi(new_rule);
                is_rule_recursive(new_rule_int, raw_rules, rec, marks);

                if (rec.at(new_rule_int)) {
                    count_rec += 1;
                }
            }
        }
    }

    rec.insert(pair<unsigned int, bool> (rule_no, count_rec > 0));
}
*/

//! Recursive check for rule 8, message is a concatenation of available patterns of rule 42.
//! Due to being a recursive method, the min_8 initial value must be zero.
bool check_new_rule_8(const string &message, const unordered_map<unsigned int, vector<string>> &rules, const unsigned long min_8) {
    // Message length.
    const unsigned long n = message.length();
    // Rule 42 patterns.
    const vector<string> c = rules.at(42);
    // Minimum pattern length among rule 42 patterns.
    unsigned long min_length;
    // Iterated pattern's length.
    unsigned long m_i;

    // printf("Current message is '%s'.\n", message.c_str());

    // Initialize minimum length if not done.
    if (min_8 == 0) {
        // Minimum length not initialized, do it once.
        min_length = n;
        for (const string &patt: c) {
            min_length = min_length < patt.length() ? min_length : patt.length();
        }
    }
    else {
        // Length was initialized, skip counting.
        min_length = min_8;
    }

    // Message is shorter than the pattern's minimum length.
    if (n < min_length) return false;

    // Iter over pattern combinations.
    for (const string &c_i: c) {
        m_i = c_i.length();

        // Pattern longer than message, invalid pattern.
        if (m_i > n) continue;

        // Check that pattern matches message start.
        if (message.substr(0, m_i) == c_i) {
            // If pattern length matches message length, pattern is message, match.
            if (m_i == n) {
                return true;
            }
            else {
                // First half matches, check the second part.
                if (check_new_rule_8(message.substr(m_i), rules, min_length)) {
                    return true;
                }
                else {
                    continue;
                }
            }
        }
        else {
            continue;
        }
    }
    return false;
}

//! Recursive check for rule 11, message is a concatenation of the same number of
//! leading rule 42 patterns and trailing rule 31 patterns .
//! Due to being a recursive method, the min_11 initial value must be zero.
bool check_new_rule_11(const string &message, const unordered_map<unsigned int, vector<string>> &rules, const unsigned long min_42, const unsigned long min_31) {
    // Message length.
    const unsigned long n = message.length();
    // Rule 42 and rule 31 patterns.
    const vector<string> c_42 = rules.at(42);
    const vector<string> c_31 = rules.at(31);
    // Minimum pattern length among rules 42 and 31 patterns.
    unsigned long min_length_42;
    unsigned long min_length_31;
    // Iterated pattern's length.
    unsigned long m_42_i;
    unsigned long m_31_i;

    // Initialize minimum length if not done.
    if (min_42 == 0) {
        // Minimum length not initialized, do it once.
        min_length_42 = n;
        for (const string &patt: c_42) {
            min_length_42 = min_length_42 < patt.length() ? min_length_42 : patt.length();
        }
    }
    else {
        // Length was initialized, skip counting.
        min_length_42 = min_42;
    }

    // Initialize minimum length if not done.
    if (min_31 == 0) {
        // Minimum length not initialized, do it once.
        min_length_31 = n;
        for (const string &patt: c_31) {
            min_length_31 = min_length_31 < patt.length() ? min_length_31 : patt.length();
        }
    }
    else {
        // Length was initialized, skip counting.
        min_length_31 = min_31;
    }

    // Message is shorter than the pattern's minimum length.
    if (n < min_length_31 + min_length_42) return false;

    // Iter over pattern combinations.
    for (const string &c_i_42: c_42) {
        for (const string &c_i_31: c_31) {
            m_31_i = c_i_31.length();
            m_42_i = c_i_42.length();

            // Pattern longer than message, invalid pattern.
            if (m_42_i + m_31_i > n) continue;

            // Check that patterns matches message start.
            if (message.substr(0, m_42_i) == c_i_42 && message.substr(n - m_31_i) == c_i_31) {
                // If pattern length matches message length, pattern is message, match.
                if (m_42_i + m_31_i == n) {
                    return true;
                }
                else {
                    // Leading and trailing sections match, check the inner part.
                    if (check_new_rule_11(message.substr(m_42_i, n - m_42_i - m_31_i), rules, min_length_42, min_length_31)) {
                        return true;
                    }
                    else {
                        continue;
                    }
                }
            }
            else {
                continue;
            }
        }
    }
    return false;
}

//! Count the number of messages in data that matches any pattern in rule 0.
unsigned long solve_2(const unordered_map<unsigned int, vector<string>> &rules, const vector<string> &messages) {
    unsigned long n = 0;
    unsigned long p;
    bool status;
    string m_l, m_t;
    for (const string &message: messages) {
        p = 1;
        status = false;
        // printf("Message '%s' is: ", message.c_str());
        while (p < message.length() - 1 && !status) {
            m_l = message.substr(0, p);
            m_t = message.substr(p);

            status = check_new_rule_8(m_l, rules, 0) && check_new_rule_11(m_t, rules, 0, 0);
            ++p;
        }

        if (status) {
            n += 1;
            // printf("'valid'\n");
        }
        else {
            // printf("'invalid'\n");
        }
    }
    return n;
}

int main() {
    unsigned long n = 0;
    // Rules map: rule_no -> rule valid patterns
    unordered_map<unsigned int, vector<string>> rules;
    // Messages.
    vector<string> data;

    // Part one.
    // Get rules: rule_no: rule_pattern
    get_rules(INPUT_PATH, &rules);
    // Get messages.
    get_data(INPUT_PATH, &data);
    // Get number of valid messages for rule 0.
    n = solve_1(rules[0], data);
    printf("Part 1: %lu\n", n);

    // Part two.
    // unordered_map<unsigned int, string> raw_rules;
    // get_raw_rules(INPUT_PATH, &raw_rules);
    // raw_rules[8] = string ("42 | 42 8");
    // raw_rules[11] = string ("42 31 | 42 11 31");
    n = solve_2(rules, data);
    printf("Part 2: %lu\n", n);


    /*
    unordered_map<unsigned int, vector<vector<string>>> rules_2;
    vector<string> data_2;
    get_rules_2(INPUT_PATH, &rules_2);
    get_data(INPUT_PATH, &data_2);
    n = solve_2(&rules_2, &data_2);
    printf("Part 2: %lu\n", n);
     */

    return 0;
}
