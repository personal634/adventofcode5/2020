//
// Created by imanol on 2/12/20.
//

#include <iostream>
#include <cstring>

using namespace std;

const char* INPUT_PATH = "../input/input05";

void get_row_column(const char* line, unsigned int* row, unsigned int* col) {
    unsigned int lbr = 0, ubr = 127, lbc = 0, ubc = 7;
    for (int i = 0; i < 10; ++i) {
        switch (*(line+i)) {
            case 'F':
                ubr = (ubr + lbr) >> 1;
                break;
            case 'B':
                lbr = (ubr + lbr + 1) >> 1;
                break;
            case 'L':
                ubc = (ubc + lbc) >> 1;
                break;
            case 'R':
                lbc = (ubc + lbc + 1) >> 1;
            default:
                break;
        }
        // cout << lbr << " " << ubr << "|";
        // cout << lbc << " " << ubc << endl;
    }
    // cout << lbr << " " << ubr << "|";
    // cout << lbc << " " << ubc << endl;
    *row = lbr;
    *col = ubc;
}

unsigned long get_max_id(const char* filename) {
    FILE* f = fopen(filename, "r");
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    unsigned long max_id = 0, c_id = 0;
    unsigned int row, col;
    char line[500];

    while( fgets(line, 100, f) ) {
        get_row_column(line, &row, &col);
        c_id = row * 8 + col;
        // cout << "r: " << row << " c: " << col << "l: " << line;
        max_id = c_id > max_id ? c_id : max_id;
    }

    fclose(f);
    return max_id;
}

unsigned long get_missing_id(const char* filename) {
    FILE* f = fopen(filename, "r");
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    unsigned long max_id = 0, c_id = 0;
    unsigned long rows_filled[128];
    unsigned int row, col, row_unfilled = 0;
    char line[500];

    for(unsigned long & i : rows_filled) i = 0;

    while( fgets(line, 100, f) ) {
        get_row_column(line, &row, &col);
        rows_filled[row] += 1 << col;
    }

    for (int i = 0; i < 128; ++i) {
        if (rows_filled[i] < (1<< 8) - 1) {
            if (i == row_unfilled || i == row_unfilled + 1) {
                row_unfilled = i;
            }
            else {
                unsigned int val = (1 << 8) - 1 - rows_filled[i];
                unsigned int c = 0;
                while (val) {
                    val = val >> 1;
                    c += 1;
                }
                c -= 1;
                max_id = i * 8 + c;
                break;
            }
        }
    }

    fclose(f);
    return max_id;
}

int main() {
    unsigned long n;

    // Part one.
    n = get_max_id(INPUT_PATH);
    printf("Part 1: %lu\n", n);

    // Part two.
    n = get_missing_id(INPUT_PATH);
    printf("Part 2: %lu\n", n);

    return 0;
}