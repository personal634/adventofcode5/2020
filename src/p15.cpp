//
// Created by imanol on 2/12/20.
//

#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

const char* INPUT_PATH = "../input/input15";
const unsigned int END_TURN_1 = 2020u;
const unsigned int END_TURN_2 = 30000000u;

unsigned long solve(const char* filename, const int n) {

    // Store existing numbers.
    unordered_map<unsigned long, pair<unsigned long, unsigned long>> num_last_turns;
    // Temporary variables.
    char line[100];
    unsigned long c_turn = 1, c_num, c_age, last_turn, temp;
    unsigned int p_c = 0, p_c_end;
    pair <unsigned long, unsigned long> p;
    string s;

    // File.
    FILE* f = fopen(filename, "r");

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    // Get first numbers and close file.
    fgets(line, 100, f);
    s = string(line);
    while (p_c_end = s.find(',', p_c + 1), p_c_end < s.size()) {
        c_num = stoul(s.substr(p_c, p_c_end - p_c));
        p = pair<unsigned long, unsigned long> (0ul, c_turn);
        num_last_turns[c_num] = p;
        p_c = p_c_end + 1;
        c_turn += 1;
    }
    c_num = stoul(s.substr(p_c));
    p = pair<unsigned long, unsigned long> (0ul, c_turn);
    num_last_turns[c_num] = p;
    c_turn += 1;
    fclose(f);

    // Iterate over existing list
    while (c_turn <= n) {
        if (p.first == 0ul) {
            c_num = 0ul;
        }
        else {
            c_num = p.second - p.first;
        }
        if (num_last_turns.count(c_num) == 0) {
            p = pair<unsigned long, unsigned long> (0ul, c_turn);
            num_last_turns[c_num] = p;
        }
        else {
            p = num_last_turns[c_num];
            p = pair<unsigned long, unsigned long> (p.second, c_turn);
            num_last_turns[c_num] = p;
        }

        c_turn += 1;
    }

    return c_num;
}

/*
unsigned long long solve_2(const char* filename) {
    // Memory variables.
    unordered_map<unsigned long, unsigned long> mem;

    unsigned long mask_ones;
    unsigned long mask_float;
    unsigned long address;
    unsigned long value;
    unsigned int p_add, p_assign, p_add_end;
    unsigned long n;
    char line[500];
    string s;
    n = 0ull;

    // File.
    FILE* f = fopen(filename, "r");

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    while (fgets(line, 500, f)) {
        s = string(line);
        p_assign = s.find('=') + 2;

        if (s.find('[') < s.size()) {
            p_add = s.find('[') + 1;
            p_add_end = s.find(']');

            address = stoul(s.substr(p_add, p_add_end - p_add));
            value = stoull(s.substr(p_assign));

            // Get the addresses.
            unordered_set<unsigned long> add_set;
            get_addresses(address, mask_ones, mask_float, &add_set);

            // Set values on addresses.
            for (unsigned long add : add_set) {
                mem[add] = value;
            }
        }
        else {
            s = s.substr(p_assign);
            get_masks(&s, &mask_ones, &mask_float);
        }
    }

    fclose(f);
    for (pair<unsigned long, unsigned long> p: mem) {
        n += p.second;
    }
    return n;
}
*/

int main() {
    unsigned long n;

    // Part one.
    n = solve(INPUT_PATH, END_TURN_1);
    printf("Part 1: %lu\n", n);

    // Part two.
    n = solve(INPUT_PATH, END_TURN_2);
    printf("Part 2: %lu\n", n);

    return 0;
}
