//
// Created by imanol on 2/12/20.
//

#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

const char* INPUT_PATH = "../input/input21";

unsigned long
count_nonallergic_ingredients(const vector<vector<string>> &ingredients, const vector<string> &allergic_ingredients);

void get_input(const char* filename, vector<vector<string>> &ingredients, vector<vector<string>> &allergens) {
    // File.
    FILE* f = fopen(filename, "r");

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    char line[800];
    string s;

    unsigned long p_par_start, p_par_end, p_con_end;

    while (fgets(line, 800, f)) {
        s = string (line);
        if (s.at(0) == '\n') continue;

        // Pointers to certain characters.
        p_par_start = s.find('(');
        p_par_end = s.find(')');
        p_con_end = p_par_start + 10;

        // Add ingredients.
        string str = s.substr(0, p_par_start - 1);
        vector<string> cont;
        unsigned long current, previous = 0;
        current = str.find(' ');
        while (current != string::npos) {
            cont.push_back(str.substr(previous, current - previous));
            previous = current + 1;
            current = str.find(' ', previous);
        }
        cont.push_back(str.substr(previous, current - previous));
        ingredients.push_back(cont);

        // Add allergens.
        str = s.substr(p_con_end, p_par_end - p_con_end);
        cont.clear();
        previous = 0;
        current = str.find(' ');
        while (current != string::npos) {
            cont.push_back(str.substr(previous, current - previous - 1));
            previous = current + 1;
            current = str.find(' ', previous);
        }
        cont.push_back(str.substr(previous, current - previous - 1));
        allergens.push_back(cont);
    }

    fclose(f);
}

void get_allergens(vector<vector<string>> &allergens, vector<string> &allergen_list) {
    unordered_set<string> a;
    for (const vector<string> &l: allergens) {
        for (const string &al: l) {
            a.insert(al);
        }
    }

    for (const string &al: a) {
        allergen_list.push_back(al);
    }
}

void get_probable_ingredients_for_allergens(vector<vector<string>> &ingredients, vector<vector<string>> &allergens, vector<string> &allergen_list, vector<unordered_set<string>> &ingredient_sets) {
    for (const string &allergen: allergen_list) {
        vector<unordered_set<string>> sets;
        for (unsigned long idx = 0; idx < ingredients.size(); ++idx) {
            vector<string> c_ingredients = ingredients.at(idx);
            vector<string> c_allergens = allergens.at(idx);

            bool matched = false;

            for (const string &i_allergen: c_allergens) {
                matched = matched || i_allergen == allergen;
            }

            if (matched) {
                unordered_set<string> set;
                for (const string &ingredient: c_ingredients) set.insert(ingredient);
                sets.push_back(set);
            }
        }

        // Get ingredients on all sets.
        unordered_set<string> all_set;
        unordered_set<string> &ref_set = sets.at(0);

        for (const string &c_ingredient: ref_set) {
            unsigned long count = 0;
            for (const unordered_set<string> &c_set: sets) {
                count += c_set.count(c_ingredient);
            }
            if (count == sets.size()) all_set.insert(c_ingredient);
        }

        ingredient_sets.push_back(all_set);
    }
}

void get_allergenic_ingredients(vector<unordered_set<string>> &ingredient_sets, vector<string> &allergenic_ingredients) {
    unsigned long n_items = 0;

    while (n_items != ingredient_sets.size()) {
        for (unsigned long t_idx = 0; t_idx < ingredient_sets.size(); ++t_idx) {

            if (ingredient_sets.at(t_idx).size() == 1) {
                string last_ing;
                for (const string &i_ing: ingredient_sets.at(t_idx)) last_ing = i_ing;
                for (unsigned long i_idx = 0; i_idx < ingredient_sets.size(); ++i_idx) {
                    if (t_idx == i_idx) continue;

                    if (ingredient_sets.at(i_idx).count(last_ing)) {
                        ingredient_sets.at(i_idx).erase(last_ing);
                    }
                }
            }
        }

        n_items = 0;
        for (const unordered_set<string> &c_set: ingredient_sets) n_items += c_set.size();
    }

    for (const unordered_set<string> &s: ingredient_sets) {
        for (const string &v: s) {
            allergenic_ingredients.push_back(v);
            break;
        }
    }
}

unsigned long count_nonallergic_ingredients(const vector<vector<string>> &ingredients, const vector<string> &allergic_ingredients) {
    unsigned long n = 0;
    unordered_set<string> set;
    for (const string &al: allergic_ingredients) set.insert(al);

    for (const vector<string> &l: ingredients) {
        for (const string &c_ing: l) {
            n += set.count(c_ing) == 0 ? 1 : 0;
        }
    }

    return n;
}

unsigned long solve_1(vector<vector<string>> &ingredients, vector<vector<string>> &allergens, vector<string> &allergen_list, vector<string> &allergic_ingredients) {
    get_allergens(allergens, allergen_list);

    vector<unordered_set<string>> ingredients_per_allergens;
    get_probable_ingredients_for_allergens(ingredients, allergens, allergen_list, ingredients_per_allergens);

    get_allergenic_ingredients(ingredients_per_allergens, allergic_ingredients);

    unsigned long n = count_nonallergic_ingredients(ingredients, allergic_ingredients);
    return n;
}



void solve_2(vector<string> &allergen_list, vector<string> &allergic_ingredients, string &list) {
    list.clear();

    for (unsigned long c_idx = 0; c_idx < allergen_list.size() - 1; ++c_idx) {
        for (unsigned long i_idx = c_idx + 1; i_idx < allergen_list.size(); ++i_idx) {
            if (allergen_list.at(c_idx) > allergen_list.at(i_idx)) {
                swap(allergen_list[c_idx], allergen_list[i_idx]);
                swap(allergic_ingredients[c_idx], allergic_ingredients[i_idx]);
            }
        }
    }

    for (const string &ing: allergic_ingredients) {
        list += ing + ',';
    }
    list.pop_back();

}

int main() {
    unsigned long n;
    vector<vector<string>> ingredients, allergens;
    vector<string> allergen_list;
    vector<string> allergic_ingredients;

    // Part one.
    get_input(INPUT_PATH, ingredients, allergens);
    n = solve_1(ingredients, allergens, allergen_list, allergic_ingredients);
    printf("Part 1: %lu\n", n);

    string list;
    solve_2(allergen_list, allergic_ingredients, list);
    printf("Part 2: '%s'\n", list.c_str());

    return 0;
}
