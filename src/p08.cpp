//
// Created by imanol on 2/12/20.
//

#include <iostream>
#include <string>
#include <unordered_set>
// #include <cstring>

using namespace std;

const char* INPUT_PATH = "../input/input08";

void reset_to_line(FILE* f, const unsigned int line_no) {
    rewind(f);
    char c[500];
    for (int i = 1; i < line_no; ++i) fgets(c, 500, f);
}

unsigned int get_line_number(FILE* f) {
    unsigned int n = 0;
    char c[500];

    rewind(f);

    while(fgets(c, 500, f)) n += 1;
    return n;
}

unsigned int execute_instructions(const char* filename, unsigned int *jmp, unsigned int swap_jmp = 0) {
    FILE* f = fopen(filename, "r");
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    unsigned int acc = 0, p_sp;
    unordered_set<unsigned int> lines;
    char line[500];
    signed int inc;
    string l, instruct;
    *jmp = 1;

    while(lines.count(*jmp) == 0) {
        // Line executed, store.
        lines.insert(*jmp);

        // Get current instruction.
        if (fgets(line, 500, f) == NULL) break;
        if (line[0] == '\n') break;
        l = string (line);

        // Find space.
        p_sp = l.find(' ');

        // Find instruction and value.
        instruct = l.substr(0, 3);
        inc = stoi(l.substr(p_sp + 1));

        if (instruct == "jmp" && *jmp == swap_jmp) {
            // Switch to nop.
            *jmp += 1;
        }
        else if (instruct == "nop" && *jmp == swap_jmp) {
            // Switch to jmp.
            *jmp += inc;
            reset_to_line(f, *jmp);
        }
        else if (instruct == "jmp") {
            *jmp += inc;
            reset_to_line(f, *jmp);
        }
        else if (instruct == "acc") {
            acc += inc;
            *jmp += 1;
        }
        else {
            *jmp += 1;
        }
    }

    fclose(f);
    return acc;
}

unsigned int swap_instructions(const char* filename) {
    FILE* f = fopen(filename, "r");
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    unsigned int acc, jmp, n = get_line_number(f), jmp_max = 0;

    for (int i = 1; i <= n; ++i) {
        jmp = 1;
        acc = execute_instructions(INPUT_PATH, &jmp, i);
        jmp_max = jmp > jmp_max ? jmp : jmp_max;
        if (jmp >= n) return acc;
    }

    // cout << jmp_max << endl;
    return 0;
}

int main() {
    unsigned long n = 0;
    unsigned int jmp = 0;

    // Part one.
    n = execute_instructions(INPUT_PATH, &jmp);
    printf("Part 1: %lu\n", n);

    // Part two.
    n = swap_instructions(INPUT_PATH);
    printf("Part 2: %lu\n", n);

    return 0;
}