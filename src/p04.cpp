//
// Created by imanol on 2/12/20.
//

#include <cstring>
#include <iostream>
#include <algorithm>
#include <regex>

using namespace std;

const char* INPUT_PATH = "../input/input04";
const char* REQUIRED_KEYS[7] = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"};

unsigned short check_key_value(const char* key, const char* value) {
    if (strcmp(key, "byr") == 0) {
        unsigned int d = stoi(value);
        if (d >= 1920 && d <= 2002) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else if (strcmp(key, "iyr") == 0) {
        unsigned int d = stoi(value);
        if (d >= 2010 && d <= 2020) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else if (strcmp(key, "eyr") == 0) {
        unsigned int d = stoi(value);
        if (d >= 2020 && d <= 2030) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else if (strcmp(key, "hgt") == 0) {
        string s (value);
        unsigned int p_cm = s.find("cm");
        unsigned int p_in = s.find("in");

        if (p_cm < s.size()) {
            unsigned int d = stoi(s.substr(0, p_cm));
            if (d >= 150 && d <= 193) {
                return 1;
            }
            else {
                return 0;
            }
        }
        else if (p_in < s.size()) {
            unsigned int d = stoi(s.substr(0, p_in));
            if (d >= 59 && d <= 76) {
                return 1;
            }
            else {
                return 0;
            }
        }
        else {
            return 0;
        }
    }
    else if (strcmp(key, "hcl") == 0) {
        if (regex_match(value, regex("#[0-9a-f]{6,6}\\s*"))) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else if (strcmp(key, "ecl") == 0) {
        if (
                regex_match(value, regex("amb\\s*")) ||
                        regex_match(value, regex("blu\\s*")) ||
                        regex_match(value, regex("brn\\s*")) ||
                        regex_match(value, regex("gry\\s*")) ||
                        regex_match(value, regex("grn\\s*")) ||
                        regex_match(value, regex("hzl\\s*")) ||
                        regex_match(value, regex("oth\\s*"))
                ) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else if (strcmp(key, "pid") == 0) {
        if (regex_match(value, regex("[0-9]{9,9}\\s*"))) {
            return 1;
        }
        else {
            return 0;
        }
    }

    return 0;
}

unsigned long count_pseudo_valid_passports(const char* filename, const unsigned int n, const char** keys, const int chk = 0) {
    FILE* f = fopen(filename, "r");
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    unsigned short i;
    unsigned long counter = 0;
    unsigned int max_key_count = 0, key_counter = 0;
    char line[500];

    for (i = 0; i < n; ++i) {
        max_key_count += 1 << i;
    }

    while( fgets(line, 100, f) ) {
        if (line[0] == '\n') {
            if (key_counter == max_key_count) counter += 1;
            key_counter = 0;
        }
        else {
            string s (line);
            unsigned int last_p = 0;
            unsigned int c_p;
            unsigned int p, p_lf = s.find('\n', 0);
            string key, val;

            while (last_p < s.size()) {
                // cout << last_p << line << endl;

                c_p = s.find(':', last_p+1);
                p = s.find(' ', last_p+1);

                if (p > p_lf) p = p_lf;

                // cout << c_p << " " << p << endl;

                if (last_p == p) break;

                key = s.substr(last_p, c_p - last_p);
                val = s.substr(c_p+1, p - c_p - 1);

                for (i = 0; i < n; ++i) {
                    if ( strcmp(keys[i], key.c_str()) == 0 ) {
                        if (chk == 0) {
                            key_counter = key_counter | (1 << i);
                        }
                        else {
                            key_counter = key_counter | (check_key_value(key.c_str(), val.c_str()) << i);
                        }

                    }
                }

                if (p >= s.size()) break;
                last_p = p + 1;
            }
        }
    }

    if (key_counter != 0) {
        if (key_counter == max_key_count) counter += 1;
        // key_counter = 0;
    }
    fclose(f);
    return counter;
}

int main() {
    unsigned long n;

    // Part one.
    n = count_pseudo_valid_passports(INPUT_PATH, 7, REQUIRED_KEYS);
    printf("Part 1: %lu\n", n);

    // Part two.
    n = count_pseudo_valid_passports(INPUT_PATH, 7, REQUIRED_KEYS, 1);
    printf("Part 2: %lu\n", n);

    return 0;
}