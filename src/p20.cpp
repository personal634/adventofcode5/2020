//
// Created by imanol on 2/12/20.
//

#include <iostream>
#include <utility>
#include <vector>

using namespace std;

const char* INPUT_PATH = "../input/input20";
const vector<pair<unsigned int, unsigned int>> SEA_MONSTER_PATTERN = {
        {0, 18},
        {1, 0},
        {1, 5},
        {1, 6},
        {1, 11},
        {1, 12},
        {1, 17},
        {1, 18},
        {1, 19},
        {2, 1},
        {2, 4},
        {2, 7},
        {2, 10},
        {2, 13},
        {2, 16}
};
const unsigned int SEA_MONSTER_PATTERN_WIDTH = 20;
const unsigned int SEA_MONSTER_PATTERN_HEIGHT = 3;


namespace tile {
    class Tile {

    public:
        unsigned int id;
        unsigned int n;
        unsigned short state;
        vector<bool> image;
        // Borders from top-left corner on a positive way.
        unsigned long borders[4]{0, 0, 0, 0};

        static unsigned long transpose(const unsigned long &v, const unsigned int &n);
        Tile(unsigned int id, unsigned int n, const vector<bool> &image);
        void rotate();
        void flip();
        void set_state(const unsigned short &s);
        void print() const;
    };

    unsigned long Tile::transpose(const unsigned long &v, const unsigned int &n) {
        unsigned long new_v = 0;
        for (unsigned int i = 0; i < n; ++i) new_v += ((v >> i) & 1) << (n - i - 1);
        return new_v;
    }

    Tile::Tile(unsigned int id, unsigned int n, const vector<bool> &image) {
        this->id = id;
        this->n = n;
        this->image = image;
        this->state = 0;

        unsigned long bs[4] = {0, 0, 0, 0};
        unsigned int row, col;

        col = 0;
        for (row = 0; row < this->n; ++row) bs[0] = bs[0] + ((this->image[col + row * this->n]) << row);

        row = this->n - 1;
        for (col = 0; col < this->n; ++col) bs[1] = bs[1] + ((this->image[col + row * this->n]) << col);

        col = this->n - 1;
        for (row = this->n - 1; row < this->n; --row) bs[2] = bs[2] + ((this->image[col + row * this->n]) << (this->n - 1 - row));

        row = 0;
        for (col = this->n - 1; col < this->n; --col) bs[3] = bs[3] + ((this->image[col + row * this->n]) << (this->n - 1 - col));

        this->borders[0] = bs[0];
        this->borders[1] = bs[1];
        this->borders[2] = bs[2];
        this->borders[3] = bs[3];
    }

    //! Rotate anti-clockwise.
    void Tile::rotate() {
        vector<bool> image2 (n * n);
        for (unsigned int i = 0; i < n; ++i) {
            for (unsigned int j = 0; j < n; ++j) {
                image2[n*(n-j-1)+i] = image.at(n*i+j);
            }
        }
        swap(image, image2);
        state = (state & 4) + (((state & 3) + 1) % 4);

        unsigned long bs[4] = {borders[3], borders[0], borders[1], borders[2]};
        swap(borders, bs);
    }

    //! Flip up-down (any flip-type is valid).
    void Tile::flip() {
        vector<bool> image2 (n * n);
        for (unsigned int i = 0; i < n; ++i) {
            for (unsigned int j = 0; j < n; ++j) {
                image2[n*(n-i-1)+j] = image.at(n*i+j);
            }
        }
        swap(image, image2);
        state ^= 4;

        unsigned long bs[4] = {this->borders[0], this->borders[1], this->borders[2], this->borders[3]};
        this->borders[0] = Tile::transpose(bs[0], this->n);
        this->borders[1] = Tile::transpose(bs[3], this->n);
        this->borders[2] = Tile::transpose(bs[2], this->n);
        this->borders[3] = Tile::transpose(bs[1], this->n);
    }

    void Tile::set_state(const unsigned short &s) {
        // Get number of anti-clockwise rotations needed for matching the new state.
        signed int n_rotations = (((s & 3) + 4 ) - (state & 3)) % 4;
        while (n_rotations > 0) {
            rotate();
            --n_rotations;
        }
        // If flip-bit mismatch, flip.
        if ((s & 4) != (state & 4)) flip();
    }

    void Tile::print() const {
        printf("Tile %u:\n", this->id);
        for (unsigned int row = 0; row < this->n; ++row) {
            for (unsigned int col = 0; col < this->n; ++col) {
                cout << (this->image[col + this->n * row] ? '#' : '.');
            }
            cout << endl;
        }
    }
}

using namespace tile;

void print_puzzle(const vector<bool> *map, const unsigned long *nx, const unsigned long *ny, const unsigned long *nz) {
    cout << "---------------" << endl;
    for (unsigned long k = 0ul; k < *nz; ++k) {
        for (unsigned long j = 0ul; j < *ny; ++j) {
            for (unsigned long i = 0ul; i < *nx; ++i) {
                cout << (map->at(k*(*nx)*(*ny)+j*(*nx)+i) ? '#' : '.');
            }
            cout << endl;
        }
        cout << endl;
    }
    cout << "---------------" << endl;
}

void get_input(const char* filename, vector<Tile> &tiles) {
    // File.
    FILE* f = fopen(filename, "r");

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    // Data declaration.
    unsigned int tile_id = 0;
    unsigned int tile_n = 0;
    vector<bool> tile_image;

    char line[100];
    string s;

    while (fgets(line, 100, f)) {
        s = string (line);

        if (s.substr(0, 5) == "Tile ") {
            tile_id = stoi(s.substr(5));
        }
        else if (s.length() <= 1) {
            Tile t = *new Tile(tile_id, tile_n, tile_image);
            tiles.push_back(t);

            tile_id = 0;
            tile_image.clear();
        }
        else {
            tile_n = s.length() - 1;
            for (const char &c: s.substr(0, tile_n)) {
                tile_image.push_back(c == '#');
            }
        }

    }

    fclose(f);
}

unsigned long solve_1(vector<Tile> &tiles) {
    unsigned int i, j, k, l, n;
    unsigned int matches;
    unsigned long b_ik, b_jl, sol = 1;

    n = tiles.at(0).n;

    // Check a tile's borders.
    for (i = 0; i < tiles.size(); ++i) {
        matches = 0;

        // Iter over other tiles.
        for (j = 0; j < tiles.size(); ++j) {
            if (i == j) continue;

            // Iter over checked tile's borders.
            for (k = 0; k < 4; ++k) {
                b_ik = tiles.at(i).borders[k];

                // Iter over the other tile's borders.
                for (l = 0; l < 4; ++l) {
                    b_jl = tiles.at(j).borders[l];

                    // Check if both borders match.
                    if (b_ik == b_jl) {
                        matches += 1;
                    }
                    // Check if checked border and the other flipped border matches.
                    if (b_ik == Tile::transpose(b_jl, n)) {
                        matches += 1;
                    }
                }
            }
        }

        // printf("Tile '%u' has '%u' matches.\n", tiles.at(i).id, matches);
        // If two borders match, the tile is a corner tile required for the answer.
        if (matches == 2) sol *= tiles.at(i).id;
    }
    return sol;
}

void arrange_tiles(vector<Tile> &tiles) {
    unsigned long n_tiles = tiles.size();
    unsigned long n_tile_rows = n_tiles;
    while (n_tile_rows * n_tile_rows > n_tiles) --n_tile_rows;
    unsigned long d, row, col, c_idx, i_row, i_col, i_idx, t_idx;
    unsigned int new_state;

    bool first_corner_found = false, matched;
    Tile *t_tile, *i_tile;

    // Find first corner and arrange it as the top-left corner.
    while (!first_corner_found) {
        unsigned int i, j, k, l, n;
        unsigned int matches;
        vector<unsigned int> matched_borders;
        unsigned long b_ik, b_jl;

        n = tiles.at(0).n;

        // Check a tile's borders.
        for (i = 0; i < tiles.size(); ++i) {
            matches = 0;
            matched_borders.clear();

            // Iter over other tiles.
            for (j = 0; j < tiles.size(); ++j) {
                if (i == j) continue;

                // Iter over checked tile's borders.
                for (k = 0; k < 4; ++k) {
                    b_ik = tiles.at(i).borders[k];

                    // Iter over the other tile's borders.
                    for (l = 0; l < 4; ++l) {
                        b_jl = tiles.at(j).borders[l];

                        // Check if both borders match.
                        if (b_ik == b_jl) {
                            matches += 1;
                            matched_borders.push_back(k);
                        }
                        // Check if checked border and the other flipped border matches.
                        if (b_ik == Tile::transpose(b_jl, n)) {
                            matches += 1;
                            matched_borders.push_back(k);
                        }
                    }
                }
            }

            // printf("Tile '%u' has '%u' matches.\n", tiles.at(i).id, matches);
            // If two borders match, the tile is a corner tile required for the answer.
            if (matches == 2) {
                first_corner_found = true;
                swap(tiles[0], tiles[i]);
                unsigned int first_border_id = ((matched_borders[0] + 1) % 4) == matched_borders[1] ? matched_borders[0] : matched_borders[1];
                signed long n_rot = (5 - first_border_id) % 4;
                while (n_rot) {
                    tiles[0].rotate();
                    --n_rot;
                }
            }
        }
    }
    // printf("Corner borders: %lu, %lu, %lu, %lu.\n", tiles[0].borders[0], tiles[0].borders[1], tiles[0].borders[2], tiles[0].borders[3]);

    // Iter over diagonal for fitting tiles below and to the right as well as next diagonal tile.
    for (d = 0; d < n_tile_rows - 1; ++d) {

        // Arrange columns on same row.
        row = d;
        for (col = d + 1; col < n_tile_rows; ++col) {
            t_idx = row * n_tile_rows + (col - 1);
            c_idx = row * n_tile_rows + col;
            t_tile = &tiles.at(t_idx);
            matched = false;

            // Iter for candidate tile;
            for (i_col = d; i_col < n_tile_rows; ++i_col) {
                for (i_row = d; i_row < n_tile_rows; ++i_row) {
                    i_idx = i_row * n_tile_rows + i_col;

                    if (i_idx <= t_idx) continue;
                    i_tile = &tiles.at(i_idx);
                    for (new_state = 0; new_state < 8; ++new_state) {
                        i_tile->set_state(new_state);

                        if (t_tile->borders[2] == Tile::transpose(i_tile->borders[0], i_tile->n)) {
                            matched = true;
                            swap(tiles[c_idx], tiles[i_idx]);
                            break;
                        }
                    }
                    if (matched) break;
                }
                if (matched) break;
            }
        }

        // Arrange rows on same column.
        col = d;
        for (row = d + 1; row < n_tile_rows; ++row) {
            t_idx = (row - 1) * n_tile_rows + col;
            c_idx = row * n_tile_rows + col;
            t_tile = &tiles.at(t_idx);
            matched = false;

            // Iter for candidate tile;
            for (i_col = d; i_col < n_tile_rows; ++i_col) {
                for (i_row = d + 1; i_row < n_tile_rows; ++i_row) {
                    i_idx = i_row * n_tile_rows + i_col;
                    if (i_col == d && i_row < row) continue;
                    // if (i_row <= d) continue;

                    i_tile = &tiles.at(i_idx);

                    for (new_state = 0; new_state < 8; ++new_state) {
                        i_tile->set_state(new_state);
                        // printf("Test ID '%u', current ID '%u', tested border '%lu', current border '%lu'\n", t_tile->id, i_tile->id, t_tile->borders[1], Tile::transpose(i_tile->borders[3], i_tile->n));
                        // printf("Borders: %lu, %lu, %lu, %lu.\n", tiles[i_idx].borders[0], tiles[i_idx].borders[1], tiles[i_idx].borders[2], tiles[i_idx].borders[3]);
                        if (t_tile->borders[1] == Tile::transpose(i_tile->borders[3], i_tile->n)) {
                            matched = true;
                            swap(tiles[c_idx], tiles[i_idx]);
                            break;
                        }
                    }
                    if (matched) break;
                }
                if (matched) break;
            }
        }

        // Arrange next diagonal testing the upper tile.
        col = d + 1;
        row = d + 1;
        t_idx = d * n_tile_rows + d + 1;
        c_idx = row * n_tile_rows + col;
        t_tile = &tiles.at(t_idx);
        matched = false;

        // Iter for candidate tile;
        for (i_col = d + 1; i_col < n_tile_rows; ++i_col) {
            for (i_row = d + 1; i_row < n_tile_rows; ++i_row) {
                i_idx = i_row * n_tile_rows + i_col;
                i_tile = &tiles.at(i_idx);
                for (new_state = 0; new_state < 8; ++new_state) {
                    i_tile->set_state(new_state);
                    if (t_tile->borders[1] == Tile::transpose(i_tile->borders[3], i_tile->n)) {
                        matched = true;
                        swap(tiles[c_idx], tiles[i_idx]);
                        break;
                    }
                }
                if (matched) break;
            }
            if (matched) break;
        }
    }
}

Tile merge_tiles(const vector<Tile> &tiles) {
    unsigned long n_tiles = tiles.size();
    unsigned long m = n_tiles;
    while (m * m > n_tiles) --m;
    unsigned long image_bit_idx, image_bit_row, image_bit_col;
    unsigned long tile_idx, tile_row, tile_col;
    unsigned long tile_image_bit_idx, tile_image_bit_row, tile_image_bit_col;
    Tile &c_tile = const_cast<Tile &>(tiles[0]);

    unsigned int id = 0;
    // Number of bits per row or column.
    unsigned int n = (tiles.at(0).n - 2) * m;
    vector<bool> image;

    for (image_bit_idx = 0; image_bit_idx < n * n; ++image_bit_idx) {
        image_bit_row = image_bit_idx / n;
        image_bit_col = image_bit_idx % n;

        tile_row = image_bit_row / (tiles.at(0).n - 2);
        tile_col = image_bit_col / (tiles.at(0).n - 2);
        tile_idx = tile_row * m + tile_col;

        tile_image_bit_row = image_bit_row - tile_row * (tiles.at(0).n - 2) + 1;
        tile_image_bit_col = image_bit_col - tile_col * (tiles.at(0).n - 2) + 1;
        tile_image_bit_idx = tile_image_bit_row * tiles.at(0).n + tile_image_bit_col;

        image.push_back(tiles.at(tile_idx).image.at(tile_image_bit_idx));
    }

    return Tile(id, n, image);
}

void find_sea_monsters(Tile &tile, unsigned int &n_sea_monsters) {
    bool matched;
    unsigned long row, col;
    n_sea_monsters = 0;
    for (unsigned short new_state = 0; new_state < 8; ++new_state) {
        tile.set_state(new_state);

        for (unsigned long row_base = 0; row_base < tile.n - SEA_MONSTER_PATTERN_HEIGHT; ++row_base) {
            for (unsigned long col_base = 0; col_base < tile.n - SEA_MONSTER_PATTERN_WIDTH; ++col_base) {
                matched = true;

                for (const pair<unsigned int, unsigned int> &x_rel: SEA_MONSTER_PATTERN) {
                    row = row_base + x_rel.first;
                    col = col_base + x_rel.second;

                    matched = matched && tile.image.at(row * tile.n + col);
                }

                if (matched) ++n_sea_monsters;
            }
        }
    }
}

unsigned long sea_roughness(const Tile &tile, const unsigned int &n_sea_monsters) {
    unsigned long n_trues = 0;
    unsigned long n_trues_near_monsters = 0;

    for (const bool &v: tile.image) if (v) ++n_trues;

    /*
    for (unsigned int row_rel = 0; row_rel < SEA_MONSTER_PATTERN_HEIGHT; ++row_rel) {
        for (unsigned int col_rel = 0; col_rel < SEA_MONSTER_PATTERN_WIDTH; ++col_rel) {
            for (const pair<unsigned long, unsigned long> &p_base: sea_monsters) {
                if (tile.image.at(tile.n * (p_base.first + row_rel) + p_base.second + col_rel)) ++n_trues_near_monsters;
            }
        }
    }
    */
    n_trues_near_monsters = n_sea_monsters * SEA_MONSTER_PATTERN.size();

    // printf("Monster size: '%lu'\n", SEA_MONSTER_PATTERN.size());
    // printf("Monsters found: '%lu'\n", n_sea_monsters);
    // printf("Monster tiles: '%lu'\n", n_trues_near_monsters);
    return n_trues - n_trues_near_monsters;
}

unsigned long solve_2(vector<Tile> &tiles) {
    unsigned long n = 0;

    arrange_tiles(tiles);
    Tile image = merge_tiles(tiles);

    unsigned int n_sea_monsters;
    find_sea_monsters(image, n_sea_monsters);
    n = sea_roughness(image, n_sea_monsters);
    return n;
}

int main() {
    unsigned long n;
    vector<Tile> tiles;

    // Part one.
    get_input(INPUT_PATH, tiles);
    n = solve_1(tiles);
    printf("Part 1: %lu\n", n);

    n = solve_2(tiles);
    printf("Part 2: %lu\n", n);

    return 0;
}
