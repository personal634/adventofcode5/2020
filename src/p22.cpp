//
// Created by imanol on 30/04/21.
//

#include <iostream>
#include <bits/stdc++.h>

using namespace std;
using Deck = deque<unsigned short>;
// using Card = unsigned short;

const char* INPUT_PATH = "../input/input22";

/*
template<class T>
void clone_vector(const vector<T> &origin, vector<T> &out) {
    out.clear();
    for (const T &v: origin) out.push_back(v);
}
*/

/*
string str_decks(const vector<unsigned int> &p1, const vector<unsigned int> &p2) {
    string s;
    for (const unsigned int &v: p1) {
        s += to_string(v) + ",";
    }
    s.pop_back();
    s += "-";
    for (const unsigned int &v: p2) {
        s += to_string(v) + ",";
    }
    s.pop_back();
    return s;

}
*/

void get_input(const char* filename, Deck &p1, Deck &p2) {
    // File.
    FILE* f = fopen(filename, "r");

    // Check file.
    if (f == nullptr) {
        printf("Error loading file.");
        exit(1);
    }

    char line[100];
    string s;

    unsigned long p_col, p_n, p_id;
    deque<unsigned short> *p_i = nullptr;

    while (fgets(line, 100, f)) {
        s = string (line);
        if (s.at(0) == '\n') {
            continue;
        }
        else if (s.at(0) == 'P') {
            p_n = s.find(' ') + 1;
            p_col = s.find(':');
            p_id = stoi(s.substr(p_n, p_col - p_n));
            p_i = p_id == 1 ? &p1 : &p2;
        }
        else if (p_i != nullptr) {
            p_i->push_back(stoi(s));
        }
    }
    fclose(f);
}

unsigned long score(const Deck &winner) {
    unsigned long n = 0;
    for (unsigned long i = 0; i < winner.size(); ++i) {
        n += winner.at(i) * (winner.size() - i);
    }
    return n;
}

unsigned long solve_1(deque<unsigned short> &p1, deque<unsigned short> &p2) {
    unsigned long n;
    unsigned short p1_c, p2_c;
    deque<unsigned short>*winner;

    while (!p1.empty() && !p2.empty()) {
        p1_c = p1.front();
        p2_c = p2.front();
        p1.pop_front();
        p2.pop_front();

        if (p1_c > p2_c) {
            p1.push_back(p1_c);
            p1.push_back(p2_c);
            winner = &p1;
        }
        else {
            p2.push_back(p2_c);
            p2.push_back(p1_c);
            winner = &p2;
        }
    }

    n = score(*winner);
    return n;
}

/*
unsigned short recursive_combat(vector<unsigned int> &p1, vector<unsigned int> &p2, unordered_set<string> &recursive_results) {
    unordered_set<string> rounds;
    unsigned int c_p1, c_p2;
    unsigned short winner = p1.size() > p2.size() ? 1 : 2;
    string round;

    while (!p1.empty() && !p2.empty()) {
        // First rule.
        round = str_decks(p1, p2);
        if (recursive_results.count(round) > 0) return 1;
        if (rounds.count(round) > 0) {
            recursive_results.insert(round);
            return 1;
        }
        rounds.insert(round);

        // Second rule.
        c_p1 = p1.at(0);
        c_p2 = p2.at(0);
        p1.erase(p1.begin());
        p2.erase(p2.begin());

        // Third rule.
        if (p1.size() >= c_p1 && p2.size() >= c_p2) {
            vector<unsigned int> d1_c, d2_c;
            clone_vector(p1, d1_c);
            clone_vector(p2, d2_c);
            winner = recursive_combat(d1_c, d2_c, recursive_results);
        }
        // Rule 4.
        else {
            winner = c_p1 > c_p2 ? 1 : 2;
        }

        if (winner == 1) {
            p1.push_back(c_p1);
            p1.push_back(c_p2);
        }
        else {
            p2.push_back(c_p2);
            p2.push_back(c_p1);
        }
    }

    return winner;
}
*/

//! True if winner is P1, else P2.
bool recursive_combat_no_mem(deque<unsigned short> &p1, deque<unsigned short> &p2) {
    set<tuple<vector<unsigned short>, vector<unsigned short>>> rounds;
    tuple<vector<unsigned short>, vector<unsigned short>> round;
    // vector<unsigned short> p1c, p2c;
    unsigned short c_p1, c_p2;
    bool winner;

    for (;;) {
        if (p1.empty()) return false;
        if (p2.empty()) return true;

        // First rule.
        vector<unsigned short> p1c (p1.size()), p2c (p2.size());
        copy(p1.begin(), p1.end(), p1c.begin());
        copy(p2.begin(), p2.end(), p2c.begin());
        round = { p1c , p2c };
        // auto ans = rounds.insert(round);
        if (!rounds.insert(round).second) return true;
        // if (rounds.count(round) > 0) return 1;
        // rounds.insert(round);

        // Second rule.
        c_p1 = p1.front();
        c_p2 = p2.front();
        p1.pop_front();
        p2.pop_front();

        // Third rule.
        if (p1.size() >= c_p1 && p2.size() >= c_p2) {
            deque<unsigned short> d1_c (c_p1), d2_c (c_p2);
            copy(p1.begin(), p1.begin()+c_p1, d1_c.begin());
            copy(p2.begin(), p2.begin()+c_p2, d2_c.begin());
            winner = recursive_combat_no_mem(d1_c, d2_c);
        }
        // Rule 4.
        else {
            winner = c_p1 > c_p2;
        }

        if (winner) {
            p1.push_back(c_p1);
            p1.push_back(c_p2);
        }
        else {
            p2.push_back(c_p2);
            p2.push_back(c_p1);
        }
    }

    // return winner;
}

unsigned long solve_2(deque<unsigned short> &p1, deque<unsigned short> &p2) {
    // unordered_set<string> rr;
    // if (recursive_combat(p1, p2, rr) == 1) {
    bool winner_is_p1 = recursive_combat_no_mem(p1, p2);
    if (winner_is_p1) {
        return score(p1);
    }
    else {
        return score(p2);
    }
}

int main() {
    unsigned long n;
    deque<unsigned short> p1, p2;

    // Part one.
    get_input(INPUT_PATH, p1, p2);
    deque<unsigned short> p11 (p1.size()), p22 (p2.size());
    copy(p1.begin(), p1.end(), p11.begin());
    copy(p2.begin(), p2.end(), p22.begin());
    //printf("Length: deck 1 -> %zu, deck 2 -> %zu.\n", p11.size(), p22.size());

    n = solve_1(p1, p2);
    printf("Part 1: %lu\n", n);

    n = solve_2(p11, p22);
    // printf("Length: deck 1 -> %zu, deck 2 -> %zu.\n", p11.size(), p22.size());
    printf("Part 2: %lu\n", n);

    return 0;
}
